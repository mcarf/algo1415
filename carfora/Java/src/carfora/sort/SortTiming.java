package carfora.sort;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Performs experiments on sorting algorithms (to be implemented in a class
 * named Sorting). Each algorithm is called on a number of randomly generated
 * integer arrays. Results are written on disk in CSV format.
 */
public class SortTiming {
  /**
   * Logger instance for the output on console.
   */
  private static final Logger log = Logger.getLogger( SortTiming.class.getName() );

  /**
   * Class for storing experiment results. An experiment is defined as a set of
   * measurement on a given array size. Each measurement is associated to a particular
   * algorithm whose name is given upon initialization.
   */
  protected static class Timings {
    public static final String SEP=",";
    /**
     * List of names of the tested algorithms
     */
    private ArrayList<String> headerFields;

    /**
     * List of array sizes used in the experiments
     */
    private ArrayList<Integer> sizes;

    /**
     * List of experiments results. Each experiment result
     *  is a list of timings (should be one for each tested algorithm).
     */
    private ArrayList<ArrayList<Double>> timings;

    /**
     * @param list the list of algorithms that are being tested
     */
    Timings(String[] list) {
      headerFields = new ArrayList<>();
      headerFields.add("Size");
      headerFields.addAll(Arrays.asList(list));

      sizes = new ArrayList<>();
      timings = new ArrayList<>();
    }

    /**
     * Create a new experiment and appends it to the experiments list.
     */
    public void addExperiment(int size) {
      sizes.add(size);
      timings.add(new ArrayList<Double>());
    }

    /**
     * Append a new timing result to the current experiment (i.e., the
     * last created one).
     * @param timing the timing to be appended
     */
    public void addTiming(double timing) {
      timings.get(timings.size()-1).add(timing);
    }

    /**
     * Joins all elements of the given array list into a
     * string. The elements will be separated by a comma symbol.
     * @param values the object whose string representation should be joined.
     */
    private <E> String join(ArrayList<E> values) {
      String result = "";
      boolean first = true;

      for(E value : values) {
        if(first) {
          first = false;
        } else {
          result += SEP;
        }
        result += value.toString();
      }

      return result;
    }

    /**
     * @return a CSV representation of the content of this object.
     */
    public String toCsvString() {
      String result = join(headerFields) + "\n";
      for( int i=0; i<sizes.size(); ++i ) {
        ArrayList<Double> timing = timings.get(i);
        result += sizes.get(i) + SEP;
        result += join(timing) + "\n";
      }

      return result;
    }
  }

  /**
   * Builds a new array of random integers. It can be subclassed
   * to vary the way the array is built.
   */
  protected static class ArrayBuilder {
    /**
     * The minimal value of integers in the built array.
     */
    protected int min;
    /**
     * The maximal value of integers in the built array.
     */
    protected int max;

    ArrayBuilder(int min, int max) {
      this.min = min;
      this.max = max;
    }

    /**
     * Create a new array of random integers. The integers put into the array
     * will vary between min and max.
     * @param size the size of the newly built array
     * @return a newly built array containing "size" random integers.
     */
    protected int[] randomArray(int size) {
      int[] array = new int[size];
      for(int i=0; i<size; ++i) {
        array[i] = min + (int)(Math.random()*max);
      }

      return array;
    }
  }

  /**
   * Calls method m on the given array and returns the elapsed time in milliseconds
   * @param m the method to be invoked
   * @param array the parameter to be passed on m invokation
   * @return the time elapsed from invokation of m(array) to its completion
   */
  protected static double testTimes(Method m, int[] array) throws ReflectiveOperationException {
    final long startTime = System.nanoTime();
    m.invoke(null, array);
    final long endTime = System.nanoTime();

    return (endTime - startTime)/1E6d;
  }

  /**
   * Run the experimentation on the algorithms specified by the algorithms parameter.
   * Each algorithm will be tested over a set of arrays whose size vary from minSize
   * to maxSize in steps of stepSize.
   * Results of the experimentation are collected into the returned Timings object.
   * @param algorithms a list containing the names of the algorithms to be tested.  It
   *     is assumed that the Sorting class contains a method for each tested algorithm and
   *     that the method name corresponds to the name used in this list.
   * @param minSize the minimum size of arrays used to test the algorithms
   * @param maxSize the maximum size of arrays used to test the algorithms
   * @param stepSize the step size to be used while generating the random arrays
   * @return a Timings class containing the results of the experimentation
   */
  static public Timings measureAlgorithms(String[] algorithms, int minSize, int maxSize, int stepSize, ArrayBuilder builder) throws ReflectiveOperationException {
    Timings timings = new Timings(algorithms);
    for( int size=minSize; size<=maxSize; size+=stepSize ) {
      log.info("starting experiment for array size:" + size);
      timings.addExperiment(size);
      int[] array = builder.randomArray(size);

      for( String algorithm : algorithms ) {
        log.info(algorithm);
        Method m = !algorithm.equals("Arrays.sort") ?
                Sorting.class.getDeclaredMethod(algorithm, int[].class) :
                Arrays.class.getDeclaredMethod("sort", int[].class);
        timings.addTiming(testTimes(m, array.clone()));
      }
    }

    return timings;
  }

  /**
   * Writes into filename the results stored in the given Timings object.
   * Results are written in CSV format.
   */
  static public void writeResults(String filename, Timings timings) throws IOException  {
    PrintWriter writer = new PrintWriter(filename, "UTF-8");
    writer.println(timings.toCsvString());
    writer.close();
  }

  // Example of using the class to test few quadratic and few optimal algorithms.
  public static void main(String[] args) throws ReflectiveOperationException, IOException {
    String[] quadratic = { "ssort", "isort", "isortBin" };
    String[] optimal = { "msortBasic", "msortNoGarbage", "msortAlt", "parallelMergesort",
      "qsortBasic", "qsortHoare", "qsortHoareIsort", "parallelQuicksort", "hsort", "Arrays.sort"};

    ArrayBuilder builder = new ArrayBuilder(Integer.MIN_VALUE, Integer.MAX_VALUE);

    writeResults("quadratic.csv", measureAlgorithms(quadratic, (int) 1E5, (int) 5E5, (int) 25E3 , builder));
    writeResults("optimal.csv", measureAlgorithms(optimal, (int) 3E6, (int) 6E7, (int) 3E6, builder));
  }
}
