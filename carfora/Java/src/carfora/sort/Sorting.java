package carfora.sort;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

/**
 * Class that contains a collections of sorting algorithms
 * @author Matteo Carfora
 */
public class Sorting {
  private void Sorting() {}

  public static boolean isSorted(int[] a) {
    for (int i = 0; i < a.length - 1; i++) {
      if (a[i] > a[i + 1]) return false;
    }
    return true;
  }

  public static <T extends Comparable<? super T>> boolean isSorted(T[] a) {
    for (int i = 0; i < a.length - 1; i++) {
      if (a[i].compareTo(a[i + 1]) > 0) return false;
    }
    return true;
  }

  /**
   * Basic implementation of selection sort with an array of integers.
   * T(n) = O(n^2) in every case
   * S(n) = O(1) in every case
   * in place and non stable
   */
  public static void ssort(int[] a) {
    for (int i = 0; i < a.length - 1; i ++) {
      int iMin = i;
      for (int j = i + 1; j < a.length; j++) {
        if (a[j] < a[iMin]) iMin = j;
      }
      int tmp = a[iMin];
      a[iMin] = a[i];
      a[i] = tmp;
    }
  }

  /**
   * Basic implementation of selection sort with a generic array.
   * T(n) = O(n^2) in every case
   * S(n) = O(1) in every case
   * in place and non stable
   */
  public static <T extends Comparable<? super T>> void ssort(T[] a) {
    for (int i = 0; i < a.length - 1; i++) {
      int iMin = i;
      for (int j = i + 1; j < a.length; j++) {
        if (a[j].compareTo(a[iMin]) < 0) iMin = j;
      }
      T tmp = a[iMin];
      a[iMin] = a[i];
      a[i] = tmp;
    }
  }

  /**
   * Basic implementation of insertion sort with an array of integers.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n^2)
   * Tbest(n) = O(n)
   * S(n) = O(1) in every case
   * in place and stable
   */
  public static void isort(int[] a) {
    for (int i = 1; i < a.length; i++) {
      int x = a[i];
      int j = i;
      while (j > 0 && x < a[j - 1]) {
        a[j] = a[j-1];
        j--;
      }
      a[j] = x;
    }
  }

  /**
   * Basic implementation of insertion sort with a generic array.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n^2)
   * Tbest(n) = O(n)
   * S(n) = O(1) in every case
   * in place and stable
   */
  public static <T extends Comparable<? super T>> void isort(T[] a) {
    for (int i = 1; i < a.length; i++) {
      T x = a[i];
      int j = i;
      while (j > 0 && x.compareTo(a[j - 1]) < 0) {
        a[j] = a[j - 1];
        j--;
      }
      a[j] = x;
    }
  }

  /**
   * Optimized binary search for insertion sort
   */
  static int rBin(int x, int[] a, int ini, int end) {
    if (x < a[ini]) return ini;
    if (x > a[end]) return end + 1;
    while (ini <= end) {
      int mid = (ini + end) >>> 1;
      if (x < a[mid]) end = mid - 1;
      else ini = mid + 1;
    }
    return ini;
  }

  /**
   * Insertion sort with binary search optimization with an array of integers.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n^2)
   * Tbest(n) = O(n)
   * S(n) = O(1) in every case
   * in place and stable
   *
   * the binary search reduces the comparisons to find the insertion place
   * within the ordered segment bringing the time complexity of the
   * comparisons from O(n) to O(log n)
   */
  public static void isortBin(int[] a) {
    int n = a.length;
    if (n == 0) return;
    int start = 1;
    while (start < n && a[start] >= a[start - 1]) start++;
    if(start == n) return;
    for (int i = start; i < n; i++) {
      int x = a[i];
      int iInser = rBin(x, a, 0, i - 1);
      for (int j = i; j > iInser; j--) {
        a[j] = a[j - 1];
      }
      a[iInser] = x;
    }
  }

  /**
   * Basic implementation of merge sort with an array of integers.
   * T(n) = O(n log n) in every case
   * S(n) = O(n) in every case
   * stable
   */
  public static void msortBasic(int[] a) {
    msortBasic(a, 0, a.length - 1);
  }

  private static void msortBasic(int[] a, int fst, int last) {
    if (fst < last) {
      int mid = (fst + last) >>> 1;
      msortBasic(a, fst, mid);
      msortBasic(a, mid + 1, last);
      merge(a, fst, mid, last);
    }
  }

  private static void merge(int[] a, int fst, int mid, int last) {
    int[] c = new int[last - fst + 1];
    int i = fst, j = mid + 1, k = 0;
    while (i <= mid && j <= last) {
      c[k++] = a[i] <= a[j] ? a[i++] : a[j++];
    }
    while (i <= mid) c[k++] = a[i++];
    while (j <= last) c[k++] = a[j++];
    for (k = 0; k < c.length; k++) {
      a[k + fst] = c[k];
    }
  }

  /**
   * Merge sort with single auxiliary array with an array of integers.
   * Tworst(n) = O(n log n)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n)
   * S(n) = O(n) in every case
   * stable
   *
   * 1 - only one n-sized auxiliary array is allocated at the start.
   * 2 - if the last number of the first segment equals or is lesser than
   *     the first number of the second segment the merge is skipped
   *     Tbest(n) = O(n).
   * 3a- when the last part of the second segment is to be merged in
   *     the auxiliary array after the first segment completes,
   *     it is not touched as it is already in the right position.
   * 3b- when the last part of the first segment is to be merged in
   *     the auxiliary array after the second segment completes, it is moved
   *     directly to the last part of the main array.
   */
  public static void msortNoGarbage(int[] a) {
    int[] aux = new int[a.length];
    msortNoGarbage(a, 0, a.length - 1, aux);
  }

  private static void msortNoGarbage(int[] a, int fst, int last, int[] aux) {
    if (fst < last) {
      int mid = (fst + last) >>> 1;
      msortNoGarbage(a, fst, mid, aux);
      msortNoGarbage(a, mid + 1, last, aux);
      if(a[mid] > a[mid + 1]) {
        mergeNoGarbage(a, fst, mid, last, aux);
      }
    }
  }

  private static void mergeNoGarbage(int[] a,
          int fst, int mid, int last, int[] aux) {
    int i, j, k;
    i = k = fst;
    j = mid + 1;
    while (i <= mid && j <= last) {
      aux[k++] = a[i] <= a[j] ? a[i++] : a[j++];
    }
    int h = mid, l = last;
    while (h >= i) a[l--] = a[h--];
    for (int r = fst; r < k; r++) {
      a[r] = aux[r];
    }
  }

  /**
   * Merge sort with single auxiliary array with a generic array.
   * Tworst(n) = O(n log n)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n)
   * S(n) = O(n) in every case
   * stable
   */
  public static <T extends Comparable<? super T>> void msortNoGarbage(T[] a) {
    Comparable[] aux = new Comparable[a.length];
    msortNoGarbage(a, 0, a.length - 1, (T[])aux);
  }

  private static <T extends Comparable<? super T>> void
      msortNoGarbage(T[] a, int fst, int last, T[] aux) {
    if (fst < last) {
      int mid = (fst + last) >>> 1;
      msortNoGarbage(a, fst, mid, aux);
      msortNoGarbage(a, mid + 1, last, aux);
      if (a[mid].compareTo(a[mid + 1]) > 0) {
        mergeNoGarbage(a, fst, mid, last, aux);
      }
    }
  }

  private static <T extends Comparable<? super T>> void
      mergeNoGarbage(T[] a, int fst, int mid, int last, T[] aux) {
    int i, j, k;
    i = k = fst;
    j = mid + 1;
    while (i <= mid && j <= last) {
      aux[k++] = a[i].compareTo(a[j]) <= 0 ? a[i++] : a[j++];
    }
    int h = mid, l = last;
    while (h >= i) a[l--] = a[h--];
    for (int r = fst; r < k; r++) {
      a[r] = aux[r];
    }
  }

  /**
   * Merge sort with swapping auxiliary array with an array of integers.
   * T(n) = O(n log n) in every case
   * S(n) = O(n) in every case
   * stable
   *
   * 1 - the main and auxiliary array roles are swapped at each level of
   *     recursion of msortAlt() leaving the copy cycle from auxiliary
   *     to main array in the merge function useless.
   * 2 - if the last number of the first segment equals or is lesser than
   *     the first number of the second segment the merge is skipped, but
   *     the copy is still needed so the time complexity doesn't change.
   */
  public static void msortAlt(int[] a) {
    int[] aux = Arrays.copyOf(a, a.length);
    msortAlt(a, 0, a.length - 1, aux);
  }

  private static void msortAlt(int[] a, int fst, int last, int[] aux) {
    if (fst < last) {
      int mid = (fst + last) >>> 1;
      msortAlt(aux, fst, mid, a);
      msortAlt(aux, mid + 1, last, a);
      if(aux[mid] <= aux[mid + 1]) {
        System.arraycopy(aux, fst, a, fst, last - fst + 1);
      }
      else {
        mergeAlt(aux, fst, mid, last, a);
      }
    }
  }

  private static void
      mergeAlt(int[] a, int fst, int mid, int last, int[] aux) {
    int i = fst, j = mid + 1, k = fst;
    while (i <= mid && j <= last) {
      aux[k++] = a[i] <= a[j] ? a[i++] : a[j++];
    }
    while (i <= mid) aux[k++] = a[i++];
    while (j <= last) aux[k++] = a[j++];
  }

  /**
   * Merge sort with swapping auxiliary array with a generic array.
   * T(n) = O(n log n) in every case
   * S(n) = O(n) in every case
   * stable
   */
  public static <T extends Comparable<? super T>> void msortAlt(T[] a) {
    Comparable[] aux = Arrays.copyOf(a, a.length);
    msortAlt(a, 0, a.length - 1, aux);
  }

  private static <T extends Comparable<? super T>> void
        msortAlt(T[] a, int fst, int last, T[] aux) {
    if (fst < last) {
      int mid = (fst + last) >>> 1;
      msortAlt(aux, fst, mid, a);
      msortAlt(aux, mid + 1, last, a);
      if(aux[mid].compareTo(aux[mid + 1]) <= 0) {
        System.arraycopy(aux, fst, a, fst, last - fst + 1);
      }
      else {
        mergeAlt(aux, fst, mid, last, a);
      }
    }
  }

  private static <T extends Comparable<? super T>> void
      mergeAlt(T[] a, int fst, int mid, int last, T[] aux) {
    int i = fst, j = mid + 1, k = fst;
    while (i <= mid && j <= last) {
      aux[k++] = a[i].compareTo(a[j]) <= 0 ? a[i++] : a[j++];
    }
    while (i <= mid) aux[k++] = a[i++];
    while (j <= last) aux[k++] = a[j++];
  }

  /**
   * Parallel version of merge sort with no garbage with an array of integers.
   * Tworst(n) = O(n log n)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n)
   * S(n) = O(n) in every case
   *
   * For each available core a thread is created (or more than 1, depending on
   * cpu architecture) and it is given a segment of the array to work on.
   *
   * The parallelism reached this way decreases the time of execution by a
   * multiplicative constant but time complexity remains the same.
   */
  public static void parallelMergesort(int[] a) {
    int n = a.length;
    int[] aux = new int[n];
    int cores = Runtime.getRuntime().availableProcessors();
    ForkJoinPool pool = new ForkJoinPool();
    ParallelMergeSorter sorter =
            new ParallelMergeSorter(a, 0, n - 1, aux, cores);
    pool.invoke(sorter);
  }

  static class ParallelMergeSorter extends RecursiveAction {
    int[] a, aux;
    int fst, last;
    int numThreads;

    ParallelMergeSorter(int[] a, int f, int l, int[] aux, int n) {
      this.a = a; this.aux = aux;
      fst = f; last = l; numThreads = n;
    }

    @Override
    protected void compute() {
      if (numThreads <= 1) msortNoGarbage(a, fst, last, aux);
      else {
        int mid = (fst + last) >>> 1;
        ParallelMergeSorter left =
                new ParallelMergeSorter(a, fst, mid, aux, numThreads/2);
        ParallelMergeSorter right =
                new ParallelMergeSorter(a, mid + 1, last, aux, numThreads/2);
        invokeAll(left, right);
        mergeNoGarbage(a, fst, mid, last, aux);
      }
    }
  }

  /**
   * Basic implementation of quick sort with an array of integers.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n log n)
   *
   * Sbest(n) = O(log n)
   * Savg(n) = O(log n)
   * Sworst(n) = O(n)
   * in place and non stable
   */
  public static void qsortBasic(int[] a) {
    qsortBasic(a, 0, a.length - 1);
  }

  private static void qsortBasic(int[] a, int inf, int sup) {
    if (inf < sup) {
      int p = a[inf], i = inf;
      for (int j = inf + 1; j <= sup; j++) {
        if (a[j] < p) {
          i++;
          int tmp = a[i];
          a[i] = a[j];
          a[j] = tmp;
        }
      }
      a[inf] = a[i];
      a[i] = p;
      qsortBasic(a, inf, i - 1);
      qsortBasic(a, i + 1, sup);
    }
  }

  /**
   * Basic implementation of quick sort with a generic array.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n log n)
   *
   * Sbest(n) = O(log n)
   * Savg(n) = O(log n)
   * Sworst(n) = O(n)
   * in place and non stable
   */
  public static <T extends Comparable<? super T>> void qsortBasic(T[] a) {
    qsortBasic(a, 0, a.length - 1);
  }

  private static <T extends Comparable<? super T>> void
        qsortBasic(T[] a, int inf, int sup) {
    if (inf < sup) {
      T p = a[inf];
      int i = inf;
      for (int j = inf + 1; j <= sup; j++) {
        if (a[j].compareTo(p) < 0) {
          i++;
          T tmp = a[i];
          a[i] = a[j];
          a[j] = tmp;
        }
      }
      a[inf] = a[i];
      a[i] = p;
      qsortBasic(a, inf, i - 1);
      qsortBasic(a, i + 1, sup);
    }
  }

  /**
   * Hoare's quick sort with an array of integers.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n log n)
   *
   * Sbest(n) = O(log n)
   * Savg(n) = O(log n)
   * Sworst(n) = O(n)
   * in place and non stable
   *
   * The pivot is not removed during partition, elements with value equal to
   * pivot can stay in one of the 2 segments leaving them more balanced.
   */
  public static void qsortHoare(int[] a) {
    qsortHoare(a, 0, a.length - 1);
  }

  private static void qsortHoare(int[] a, int inf, int sup) {
    if (inf < sup) {
      int x = a[inf], i = inf, j = sup;
      do {
        while (a[i] < x) i++;
        while (a[j] > x) j--;
        if (i <= j) {
          int tmp = a[i];
          a[i] = a[j];
          a[j] = tmp;
          i++; j--;
        }
      } while (i <= j);
      qsortHoare(a, inf, j);
      qsortHoare(a, i, sup);
    }
  }

  /**
   * Hoare's quick sort with a generic array.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n log n)
   *
   * Sbest(n) = O(log n)
   * Savg(n) = O(log n)
   * Sworst(n) = O(n)
   * in place and non stable
   */
  public static <T extends Comparable<? super T>> void qsortHoare(T[] a) {
    qsortHoare(a, 0, a.length - 1);
  }

  private static <T extends Comparable<? super T>> void
        qsortHoare(T[] a, int inf, int sup) {
    if (inf < sup) {
      T x = a[inf];
      int i = inf, j = sup;
      do {
        while (a[i].compareTo(x) < 0) i++;
        while (a[j].compareTo(x) > 0) j--;
        if (i <= j) {
          T tmp = a[i];
          a[i] = a[j];
          a[j] = tmp;
          i++; j--;
        }
      } while (i <= j);
      qsortHoare(a, inf, j);
      qsortHoare(a, i, sup);
    }
  }

  /**
   * Quick sort with various optimizations with an array of integers.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n log n)
   *
   * Sbest(n) = O(1)
   * Savg(n) = O(log n)
   * Sworst(n) = O(log n)
   * in place and non stable
   *
   * 1 - Hoare's partitioning (pivot is not removed and can stay anywhere).
   * 2 - Pivot is middle element (ordered array best temporal case)
   * 3 - Tail recursion is removed for the greater's segment call, many less
   *     recursive calls will be made, improving spatial complexity.
   * 4 - Under some calculated segment threshold quicksort will not continue
   *     recursion. After the main quicksort is done the array will be divided
   *     in many "semi-ordered" segments and an insertion sort will start
   *     and sort them.
   */
  public static void qsortHoareIsort(int[] a) {
    qsortHoareIsort(a, 0, a.length - 1);
    isort(a);
  }

  private static void qsortHoareIsort(int[] a, int inf, int sup) {
      while (sup - inf > 5) {
      int[] indices = partition(a, inf, sup);
      int i = indices[0], j = indices[1];
      if (j - inf < sup - i) {
        qsortHoareIsort(a, inf, j);
        inf = i;
      } else {
        qsortHoareIsort(a, i, sup);
        sup = j;
      }
    }
  }

  /**
   * Hoare's partitioning
   * @return array that contains indices final position
   */
  private static int[] partition(int[] a, int inf, int sup) {
    int i = inf, j = sup;
    if (inf < sup) {
      // pivot is middle element
      int x = a[(inf + sup) >>> 1];
      do {
        while (a[i] < x) i++;
        while (a[j] > x) j--;
        if (i <= j) {
          int tmp = a[i];
          a[i] = a[j];
          a[j] = tmp;
          i++; j--;
        }
      } while (i <= j);
    }
    return new int[]{i, j};
  }

  /**
   * Quick sort with various optimizations with a generic array.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n log n)
   *
   * Sbest(n) = O(1)
   * Savg(n) = O(log n)
   * Sworst(n) = O(log n)
   * in place and non stable
   */
  public static <T extends Comparable<? super T>> void qsortHoareIsort(T[] a) {
    qsortHoareIsort(a, 0, a.length - 1);
    isort(a);
  }

  private static <T extends Comparable<? super T>> void
        qsortHoareIsort(T[] a, int inf, int sup) {
    while (sup - inf > 5) {
      T x = a[(inf + sup) >>> 1];
      int i = inf, j = sup;
      do {
        while (a[i].compareTo(x) < 0) i++;
        while (a[j].compareTo(x) > 0) j--;
        if (i <= j) {
          T tmp = a[i];
          a[i] = a[j];
          a[j] = tmp;
          i++; j--;
        }
      } while (i <= j);
      if (j - inf < sup - i) {
        qsortHoareIsort(a, inf, j);
        inf = i;
      } else {
        qsortHoareIsort(a, i, sup);
        sup = j;
      }
    }
  }

  /**
   * Parallel version of Hoare's quick sort with an array of integers.
   * Tworst(n) = O(n^2)
   * Tavg(n) = O(n log n)
   * Tbest(n) = O(n log n)
   *
   * Sbest(n) = O(1)
   * Savg(n) = O(log n)
   * Sworst(n) = O(log n)
   *
   * For each available core a thread is created (or more than 1, depending on
   * cpu architecture) and it is given a segment of the array to work on.
   *
   * The parallelism reached this way decreases the time of execution by a
   * multiplicative constant but time complexity remains the same.
   */
  public static void parallelQuicksort(int[] a) {
    int n = a.length;
    int cores = Runtime.getRuntime().availableProcessors();
    ForkJoinPool pool = new ForkJoinPool();
    ParallelQuickSorter sorter =
            new ParallelQuickSorter(a, 0, n - 1, cores);
    pool.invoke(sorter);
    isort(a);
  }

  static class ParallelQuickSorter extends RecursiveAction {
    int[] a;
    int fst, last;
    int numThreads;

    ParallelQuickSorter(int[] a, int f, int l, int n) {
      this.a = a;
      fst = f; last = l; numThreads = n;
    }

    @Override
    protected void compute() {
      if (numThreads <= 1) qsortHoareIsort(a, fst, last);
      else {
        int[] indices = partition(a, fst, last);
        int i = indices[0], j = indices[1];
        ParallelQuickSorter left =
                new ParallelQuickSorter(a, fst, j, numThreads/2);
        ParallelQuickSorter right =
                new ParallelQuickSorter(a, i, last, numThreads/2);
        invokeAll(left, right);
      }
    }
  }

  /**
   * Heap sort with an array of integers
   * T(n) = O(n log n) in every case
   * S(n) = O(1) in every case
   */
  public static void hsort(int[] a) {
    int n = a.length;
    // heapify
    for (int j = (n - 2) / 2; j >= 0; j--)
      moveDown(a, j, n);
    for (int i = n - 1; i > 0; i--) {
      // estraiMax
      int tmp = a[0];
      a[0] = a[i];
      a[i] = tmp;
      moveDown(a, 0, i);
    }
  }

  private static void moveDown(int[] a, int i, int size) {
    int elem = a[i];
    int j;
    while ((j = 2*i + 1) < size) {
      if (j + 1 < size && a[j + 1] > a[j]) j++;
      if (elem >= a[j]) break;
      a[i] = a[j];
      i = j;
    }
    a[i] = elem;
  }

  /**
   * Heap sort with a generic array
   * T(n) = O(n log n) in every case
   * S(n) = O(1) in every case
   */
  public static <T extends Comparable<? super T>> void hsort(T[] a) {
    int n = a.length;
    // heapify
    for (int j = (n - 2) / 2; j >= 0; j--)
      moveDown(a, j, n);
    for (int i = n - 1; i > 0; i--) {
      // estraiMax
      T tmp = a[0];
      a[0] = a[i];
      a[i] = tmp;
      moveDown(a, 0, i);
    }
  }

  private static <T extends Comparable<? super T>> void moveDown(T[] a,
          int i, int size) {
    T elem = a[i];
    int j;
    while ((j = 2*i + 1) < size) {
      if (j + 1 < size && a[j + 1].compareTo(a[j]) > 0) j++;
      if (elem.compareTo(a[j]) >= 0) break;
      a[i] = a[j];
      i = j;
    }
    a[i] = elem;
  }
}