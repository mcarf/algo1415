package carfora.binarysearch;

import java.util.Arrays;

/**
 * Implementation of a sorted dynamic array of integers
 * @author Matteo Carfora
 */

public class IntSortedArray {
  int[] elements;
  int size;

  public IntSortedArray() {
    elements = new int[16];
    size = 0;
  }

  public IntSortedArray(int initialCapacity) {
    if (initialCapacity < 0) throw new IllegalArgumentException();
    else {
      elements = new int[initialCapacity];
      size = 0;
    }
  }

  public IntSortedArray(int[] a) {
    size = a.length;
    elements = new int[size + 16];
    for (int i = 0; i < size; i++) {
      elements[i] = a[i];
    }
    Arrays.sort(elements, 0, size);   // sorting of array filled segment
  }

  public int size() {
    return size;
  }

  int binarySearch(int x) {
    int inf = 0, sup = size - 1;  // indices init
    // empty array and first element optimization checks
    if (sup == -1 || x < elements[0]) return -1;
    // last element optimization check
    if (x > elements[sup]) return -(size + 1);
    while(inf <= sup) {
      // median index calculation with zero extension shift
      int i = (inf + sup) >>> 1;
      // x if found must be in range [inf, i-1]
      if (x < elements[i]) sup = i - 1;
      // x if found must be in range [i+1, sup]
      else if (x > elements[i]) inf = i + 1;
      // x found
      else return i;
    }
    return -(inf + 1);
  }

  void reallocate() {
    int[] newElements = new int[elements.length * 2 + 1];
    for (int i = 0; i < size; i++) {
      newElements[i] = elements[i];
    }
    elements = newElements;
  }

  public int insert(int x) {
    int pos = binarySearch(x);
    if (pos < 0) pos = -(pos + 1);    // x not found
    if (size == elements.length) reallocate(); // array size check
    // shift elements to the right after pos
    for (int i = size - 1; i >= pos; i--) {
      elements[i + 1] = elements[i];
    }
    elements[pos] = x;
    size++;
    return pos;
  }

  public int indexOf(int x) {
    int result = binarySearch(x);
    return result < 0 ? -1 : result;
  }

  public int get(int i) {
    if (i < 0 || i >= size) throw new ArrayIndexOutOfBoundsException();
    return elements[i];
  }

  @Override
  public String toString() {
    String content = "[";
    content += (size > 0 ? elements[0] : "");   // first element check
    for (int i = 1; i < size; i++) {
      content += ", " + elements[i];  // padding + current
    }
    content += "]";
    return content;
  }
}
