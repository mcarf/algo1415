package carfora.binarysearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Implementation of a generic sorted dynamic array
 * @author Matteo Carfora
 *
 * @param <E> class of the sorted elements
 */

public class SortedArrayList<E extends Comparable<? super E>> {
  ArrayList<E> elements;

  public SortedArrayList() {
    elements = new ArrayList<>(16);
  }

  public SortedArrayList(int initialCapacity) {
    elements = new ArrayList<>(initialCapacity);
  }

  public SortedArrayList(E[] a) {
    elements = new ArrayList<>(a.length + 16);
    elements.addAll(Arrays.asList(a));
    Collections.sort(elements);
  }

  public int size() {
    return elements.size();
  }

  int binarySearch(E x) {
    int inf = 0, sup = elements.size()- 1;  // indices init
    // empty array and first element optimization checks
    if (sup == -1 || x.compareTo(elements.get(0)) < 0) return -1;
    // last element optimization check
    if (x.compareTo(elements.get(sup)) > 0) return -(elements.size() + 1);
    while(inf <= sup) {
      // median index calculation with zero extension shift
      int i = (inf + sup) >>> 1;
      // x if found must be in range [inf, i-1]
      if (x.compareTo(elements.get(i)) < 0) sup = i - 1;
      // x if found must be in range [i+1, sup]
      else if (x.compareTo(elements.get(i)) > 0) inf = i + 1;
      // x found
      else return i;
    }
    return -(inf + 1);
  }

  public int insert(E x) {
    int pos = binarySearch(x);
    if (pos < 0) pos = -(pos + 1);  // x not found
    elements.add(pos, x);
    return pos;
  }

  public int indexOf(E x) {
    int result = binarySearch(x);
    return result < 0 ? -1 : result;
  }

  public E get(int i) {
    return elements.get(i);
  }

  @Override
  public String toString() {
    return elements.toString();
  }
}
