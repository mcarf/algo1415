package carfora.huffman;

import carfora.priorityqueue.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Huffman Coding implementation
 * @author Matteo Carfora
 * @param <T> type of objects to be encoded
 */
public class HuffmanCoding<T> {

  class Node {
    T item;
    double freq;
    Node left, right;

    Node(T item, double freq) {
      this.item = item;
      this.freq = freq;
    }

    Node(Node left, Node right) {
      this.item = null;
      this.left = left;
      this.right = right;
      this.freq = left.freq + right.freq;
    }
  }

  // key is object to be encoded, value is huffman code
  HashMap<T, String> huffMap;
  Node root;

  private void huffMapBuild(Node nd, String buff) {
    if (nd.item != null) huffMap.put(nd.item, buff);
    else {
      huffMapBuild(nd.left, buff + "0");
      huffMapBuild(nd.right, buff + "1");
    }
  }

  /**
   * Builds a compressing code based on the statistics reported
   * by the given frequencies.
   * @param frequencies
   */
  public void build(HashMap<T, Double> frequencies) {
    huffMap = new HashMap<>();
    PriorityQueue<Node, Double> q = new PriorityQueueHeap<>();
    for (Map.Entry<T, Double> entry : frequencies.entrySet()) {
      double freq = entry.getValue();
      q.add(new Node(entry.getKey(), freq), freq);
    }
    for (int i = 0; i < frequencies.size() - 1; i++) {
      Node x = q.removeFirst();
      Node y = q.removeFirst();
      Node z = new Node(x, y);
      q.add(z, z.freq);
    }
    root = q.removeFirst();
    huffMapBuild(root, "");
  }

  /**
   * Encodes all objects in source.
   * @param source an array list of the objects to be encoded.
   * @return a binary representation of the encoded source.
   */
  public BitArray encode(ArrayList<T> source) {
    BitArray arr = new BitArray(32);
    for (T item : source) {
      for (char c : huffMap.get(item).toCharArray()) {
        arr.add(c == '0' ? 0 : 1);
      }
    }
    return arr;
  }

  /**
   * Decodes the objects in source into the output array.
   * @param source a binary representation of the encoded file
   * @return an array containing the decoded objects
   */
  public ArrayList<T> decode(BitArray source) {
    ArrayList<T> ls = new ArrayList<>();
    Node iter = root;
    for (int i = 0; i < source.size(); i++) {
      if (source.get(i) == 0)
        iter = iter.left;
      else
        iter = iter.right;
      if (iter.item != null) {
        ls.add(iter.item);
        iter = root;
      }
    }
    return ls;
  }
}
