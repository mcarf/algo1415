package carfora.huffman;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author roberto.esposito@unito.it
 * Utilities for the coding exercise.
 */
public abstract class CodingUtils {

  /**
   * Loads the contents of the file located at the given path and returns
   * it as an array of bytes.
   * @param path the path to the file to be loaded.
   * @return the content of the file as an array of bytes.
   * @throws IOException in case IO errors occur.
   */
  public static ArrayList<Byte> loadBytesFromFileNamed(String path) throws IOException {
    byte[] encoded = Files.readAllBytes(Paths.get(path));
    ArrayList<Byte> result = new ArrayList<>();

    for(int i=0; i<encoded.length; ++i) {
      result.add(encoded[i]);
    }

    return result;
  }

  /**
   * Load the contents of the file located at the given path. This procedure assumes
   * that the file contains the binary representation of a compressed source created
   * using saveBitsIntoFileNamed. The first 32 bits in the file are assumed to contain
   * the length in bits of the remaining data.
   * @param path to the file to be read
   * @return a binary representation of the content of the file
   * @throws IOException in case an IO error occurs.
   */
  public static BitArray loadBitsFromFileNamed(String path) throws IOException {
    BitArray result = new BitArray(1000);
    FileInputStream inFile = new FileInputStream(path);

    int len = inFile.read() & 0x000000FF;
    len |= inFile.read() << 8;
    len |= inFile.read() << 16;
    len |= inFile.read() << 24;

    int current = inFile.read();
    while(len > result.size() && current!=-1) {
      setBits(result, (byte) current, len);
      current = inFile.read();
    }

     inFile.close();
     return result;
  }

  public static BitArray loadBitsFromFileNamed2(String path) throws IOException {
    BitArray result = new BitArray(1000);
    byte[] bytes = Files.readAllBytes(Paths.get(path));
    ByteBuffer buf = ByteBuffer.wrap(bytes);
    int len = buf.getInt();

    for (int i = 0; i < len; i += 8) {
      setBits(result, buf.get(), len);
    }
    return result;
  }

  /**
   * Returns a table of the frequencies with which each byte occurs in bytes.
   * @param bytes the data to be analysed.
   * @return an HashMap representing the frequencies table.
   */

  public static HashMap<Byte, Double> statsForBytes(ArrayList<Byte> bytes) {
    int sum = 0;
    HashMap<Byte, Double> result = new HashMap<>();
    for(Byte b : bytes) {
      double value = result.containsKey(b) ? result.get(b) + 1.0 : 1.0;
      result.put(b, value);
      sum += 1;
    }

    for( Byte b: result.keySet() ) {
      result.put(b, result.get(b) / sum);
    }

    return result;
  }

  /**
   * Save binary data "bits" into a file located at the given path. The first 32 bits
   * of the file are set to the number of bits that will follow.
   * @param path to the file to be created
   * @param bits to be written
   * @throws IOException in case an IO error occurs
   */
  public static void saveBitsIntoFileNamed(String path, BitArray bits) throws IOException {
    FileOutputStream outFile = new FileOutputStream(path);
    int numBits = bits.size();
    outFile.write(numBits & 0x000000FF);
    outFile.write((numBits & 0x0000FF00) >> 8);
    outFile.write((numBits & 0x00FF0000) >> 16);
    outFile.write((numBits & 0xFF000000) >> 24);
    int i = 0;
    while(i<bits.size()) {
      outFile.write(getByte(bits, i));
      i+=8;
    }
    outFile.close();
  }

  public static void saveBitsIntoFileNamed2(String path, BitArray bits) throws IOException {
    // (total bits + int (32 bits) + 7) / byte_size
    byte[] bytes = new byte[(bits.size() + 39) >>> 3];
    ByteBuffer buf = ByteBuffer.wrap(bytes);
    buf.putInt(bits.size());
    for (int i = 0; i<bits.size(); i += 8) {
      buf.put(getByte(bits, i));
    }
    Files.write(Paths.get(path), bytes);
  }
  /**
   * Saves bytes from the given array into a file located at the given path.
   * @param path the location of the file to be written
   * @param bytes the data to be written
   * @throws IOException in case an IO error occurs.
   */
  public static void saveBytesToFileNamed(String path, ArrayList<Byte> bytes) throws IOException {
    OutputStream outFile = new FileOutputStream(path);
    for( Byte b : bytes ) {
      outFile.write(b);
    }

    outFile.close();
  }

  public static void saveBytesToFileNamed2(String path, ArrayList<Byte> bytes) throws IOException {
    byte[] b = new byte[bytes.size()];
    for (int i = 0; i < b.length; i++)
      b[i] = bytes.get(i);

    Files.write(Paths.get(path), b);
  }

  static private byte getByte(BitArray bits, int index) {
    byte result = 0;
    int end = Math.min(8, bits.size() - index );
    for(int i=0; i<end; ++i) {
      int bit = bits.get(index+i);
      result |= (bit << 7-i);
    }

    return result;
  }

  static private void setBits(BitArray container, byte current, int maxLen) {
    for(int i=0; i<8 && container.size() < maxLen; ++i) {
      int currentBit = ((current >> (7-i)) & 1);
      container.add( currentBit );
    }
  }


}