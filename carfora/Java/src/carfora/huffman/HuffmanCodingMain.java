package carfora.huffman;

import java.io.IOException;
import java.util.ArrayList;

public class HuffmanCodingMain {

  /**
   * This program tests HuffmanCoding against data provided by the user.
   * It performs the following:
   *   - Reads args[0] from disk
   *   - encodes the read data using HuffmanCoding
   *   - writes the encoded data to a file named args[0]+".encoded"
   *   - read back the encoded data
   *   - writes the decoded data to a file named args[0]+".decoded"
   * Note: in case of large files consider augmenting the heap size using the
   *     flag: -Xmx1500m
   *
   * @param args[0] needs to be the name of the file to be processed.
   * @throws IOException in case an IO error occurs
   */

  public static void main(String[] args) throws IOException {
    if(args.length != 1) {
      System.out.println("Please usage: HuffmanCodingMain <inputFileName>");
      System.exit(1);
    }

    String filename = args[0];

    HuffmanCoding<Byte> hc = new HuffmanCoding<>();

    System.out.println("Loading data...");
    ArrayList<Byte> bytes = CodingUtils.loadBytesFromFileNamed(filename);

    System.out.println("Computing statistics");
    hc.build(CodingUtils.statsForBytes(bytes));


    System.out.println("Encoding compressed data");
    BitArray compressedData = hc.encode(bytes);

    System.out.println("Saving compressed data");
    //CodingUtils.saveBitsIntoFileNamed(filename + ".compressed", compressedData);
    CodingUtils.saveBitsIntoFileNamed2(filename + ".compressed", compressedData);

    System.out.println("Loading back compressed data");
    //compressedData = CodingUtils.loadBitsFromFileNamed(filename + ".compressed");
    compressedData = CodingUtils.loadBitsFromFileNamed2(filename + ".compressed");

    System.out.println("Decoding compressed data");
    ArrayList<Byte> decompressedData = hc.decode(compressedData);

    System.out.println("Saving decompressed data");
    //CodingUtils.saveBytesToFileNamed(filename + ".decompressed", decompressedData);
    CodingUtils.saveBytesToFileNamed2(filename + ".decompressed", decompressedData);

    System.out.println("Done.");
  }

}
