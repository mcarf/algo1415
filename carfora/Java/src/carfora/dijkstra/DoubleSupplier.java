package carfora.dijkstra;

/**
 * Functional interface (Java 8) of a method that returns a double
 * this interface should be implemented by the edge class
 * @author Matteo Carfora
 */
public interface DoubleSupplier {
  double getAsDouble();
}
