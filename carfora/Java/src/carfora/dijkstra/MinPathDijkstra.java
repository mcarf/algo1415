package carfora.dijkstra;

import carfora.graphs.Graph;
import carfora.priorityqueue.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of Dijkstra's algorithm of minimum path between 2 nodes
 * @author Matteo Carfora
 * @param <V> type of nodes in the graph
 * @param <E> type of edges in the graph, it supplies a double value
 */
public class MinPathDijkstra<V, E extends DoubleSupplier> {

  private HashMap<V, Double> dist;
  private HashMap<V, V> parent;
  private PriorityQueue<V, Double> q;

  // variables initialization
  private void init(Graph<V,E> graph, V source) {
    dist = new HashMap<>();
    parent = new HashMap<>();
    q = new PriorityQueueHeap<>();
    for (V vertex : graph.getVertices()) {
      dist.put(vertex, Double.POSITIVE_INFINITY);
      q.add(vertex, Double.POSITIVE_INFINITY);
    }
    parent.put(source, source);
    dist.put(source, 0.0);
    q.setPriority(source, 0.0);
  }

  /**
   * Given the initial and final node and the minimum path tree
   * returns the path from source to dest if it exists
   * @param parent the minimum path tree (value is parent of key)
   * @param source the initial node
   * @param dest the final node
   * @return a list of nodes with the minimum path if it exists,
   * null otherwise
   */
  private List<V> pathSelection(HashMap<V,V> parent, V source, V dest) {
    LinkedList<V> path = new LinkedList<>();
    V iter = parent.get(dest);
    if (iter != null) {
      path.addFirst(dest);
      while (!iter.equals(source)) {
        path.addFirst(iter);
        iter = parent.get(iter);
      }
      path.addFirst(source);
      return path;
    }
    return null;
  }

  /**
   * Minimum path between 2 nodes (Dijkstra's algorithm)
   * @param graph the graph where the path should be searched
   * @param source the initial node
   * @param dest the final node
   * @return a list of nodes with the minimum path if it exists,
   *  null otherwise
   */
  public List<V> minPath(Graph<V,E> graph, V source, V dest) {
    init(graph, source);
    // min path tree construction stops if source-dest path is found
    while (!q.isEmpty() && !q.first().equals(dest)) {
      V vertex = q.removeFirst();
      for (V adj : graph.getNeighbors(vertex)) {
        double vCost = dist.get(vertex);
        double archCost = graph.getData(vertex, adj).getAsDouble();
        if (vCost + archCost < dist.get(adj)) {
          parent.put(adj, vertex);
          dist.put(adj, vCost + archCost);
          q.setPriority(adj, vCost + archCost);
        }
      }
    }
    return pathSelection(parent, source, dest);
  }
}
