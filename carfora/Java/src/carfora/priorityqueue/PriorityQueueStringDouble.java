package carfora.priorityqueue;

/**
 * Priority queue interface with String elements with double priority
 * @author Matteo Carfora
 */
public interface PriorityQueueStringDouble {

  /**
   * Inserts an element in the queue
   * @param element element to be inserted
   * @param priority priority of the element
   * @return false if element is already present, true if insertion goes well.
   */
  boolean add(String element, double priority);

  /**
   * Gets the element with the highest priority
   * @return the highest priority element, null otherwise
   */
  String first();

  /**
   * Removes and returns the element with the highest priority
   * @return the highest priority element extracted, null otherwise
   */
  String removeFirst();

  /**
   * Checks if queue is empty
   * @return true if empty, false otherwise
   */
  boolean isEmpty();

  /**
   * Deletes an element in the queue
   * @param element the element to be deleted
   * @return true if deleted, false otherwise
   */
  boolean delete(String element);

  /**
   * Changes priority of an element in the queue
   * @param element the element to be modified
   * @param priority the new priority
   * @return true if priority changed, false otherwise
   */
  boolean setPriority(String element, double priority);
}
