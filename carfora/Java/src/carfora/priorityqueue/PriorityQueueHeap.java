package carfora.priorityqueue;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Implementation of PriorityQueue with an heap structure
 * @author Matteo Carfora
 */
public class PriorityQueueHeap<E, P extends Comparable<? super P>> implements
        PriorityQueue<E, P> {

  class ElemWithPrior {
    E element;
    P priority;

    public ElemWithPrior(E element, P priority) {
      this.element = element;
      this.priority = priority;
    }
  }
  private ElemWithPrior[] heap;
  private int size;
  private final HashMap<E, Integer> position;

  public PriorityQueueHeap() {
    heap = (ElemWithPrior[]) Array.newInstance(ElemWithPrior.class, 16);
    size = 0;
    position = new HashMap<>();
  }

  public PriorityQueueHeap(int initialCapacity) {
    heap = (ElemWithPrior[])
            Array.newInstance(ElemWithPrior.class, initialCapacity);
    size = 0;
    position = new HashMap<>();
  }

  @Override
  public boolean add(E element, P priority) {
    if (size == heap.length) reallocate();
    if(position.get(element) == null) {
      heap[size++] = new ElemWithPrior(element, priority);
      position.put(element, size - 1);
      moveUp(size - 1);
      return true;
    }
    return false;
  }

  @Override
  public E first() {
    if (size == 0) throw new IllegalStateException("Queue is empty");
    return heap[0].element;
  }

  @Override
  public E removeFirst() {
    if (size == 0) throw new IllegalStateException("Queue is empty");
    E first = heap[0].element;
    ElemWithPrior last = heap[--size];
    if (size > 0) {
      heap[0] = last;
      moveDown(0);
    }
    return first;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public boolean delete(E element) {
    Integer index = position.get(element);
    if (index != null) {
      ElemWithPrior last = heap[--size];
      if (size > 0) {
        heap[index] = last;
        moveDown(index);
      }
      position.remove(element);
      return true;
    }
    return false;
  }

  @Override
  public boolean setPriority(E element, P priority) {
    Integer index = position.get(element);
    if (index != null) {
      heap[index].priority = priority;
      moveDown(index);
      moveUp(index);
      return true;
    }
    return false;
  }

  private void moveUp(int i) {
    ElemWithPrior s = heap[i];
    while (i > 0 && s.priority.compareTo(heap[(i - 1) / 2].priority) < 0) {
      heap[i] = heap[(i - 1) / 2];
      position.put(heap[i].element, i);
      i = (i - 1) / 2;
    }
    heap[i] = s;
    position.put(s.element, i);
  }

  private void moveDown(int i) {
    ElemWithPrior s = heap[i];
    int j;
    while ((j = 2*i + 1) < size) {
      if (j + 1 < size
              && heap[j + 1].priority.compareTo(heap[j].priority) < 0) j++;
      if (s.priority.compareTo(heap[j].priority) <= 0) break;
      heap[i] = heap[j];
      position.put(heap[i].element, i);
      i = j;
    }
    heap[i] = s;
    position.put(s.element, i);
  }

  private void reallocate() {
    heap = Arrays.copyOf(heap, heap.length * 2 + 1);
  }
}
