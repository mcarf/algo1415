package carfora.priorityqueue;

import java.util.Arrays;

/**
 * Priority queue implementation with heap structure and
 * integer elements in a limited range
 * @author Matteo Carfora
 */
public class PriorityQueueIntDouble {

  class IntWithPrior {
    int element;
    double priority;

    public IntWithPrior(int element, double priority) {
      this.element = element;
      this.priority = priority;
    }
  }

  private IntWithPrior[] heap;
  private int size;
  private final int[] position;

  /**
   * Queue constructor with integer elements range between 0 - (n-1)
   * @param n superior limit
   */
  public PriorityQueueIntDouble(int n) {
    heap = new IntWithPrior[16];
    size = 0;
    position = new int[n];
    Arrays.fill(position, -1);
  }

  public boolean add(int element, double priority) {
    if (size == heap.length) reallocate();
    if (position[element] == -1) {
      heap[size++] = new IntWithPrior(element, priority);
      position[element] = size - 1;
      moveUp(size - 1);
      return true;
    }
    return false;
  }

  public int first() {
    if (size == 0) throw new IllegalStateException("Queue is empty");
    return heap[0].element;
  }

  public int removeFirst() {
    if (size == 0) throw new IllegalStateException("Queue is empty");
    int first = heap[0].element;
    IntWithPrior last = heap[--size];
    if (size > 0) {
      heap[0] = last;
      moveDown(0);
    }
    return first;
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public boolean delete(int element) {
    int i = position[element];
    if (i != -1) {
      IntWithPrior last = heap[--size];
      if (size > 0) {
        heap[i] = last;
        moveDown(i);
      }
      position[element] = -1;
      return true;
    }
    return false;
  }

  public boolean setPriority(int element, double priority) {
    int i = position[element];
    if (i != -1) {
      heap[i].priority = priority;
      moveDown(i);
      moveUp(i);
      return true;
    }
    return false;
  }

  private void moveUp(int i) {
    IntWithPrior s = heap[i];
    while (i > 0 && s.priority < heap[(i - 1) / 2].priority) {
      heap[i] = heap[(i - 1) / 2];
      position[heap[i].element] = i;
      i = (i - 1) / 2;
    }
    heap[i] = s;
    position[s.element] = i;
  }

  private void moveDown(int i) {
    IntWithPrior s = heap[i];
    int j;
    while ((j = 2*i + 1) < size) {
      if (j + 1 < size
              && heap[j + 1].priority < heap[j].priority) j++;
      if (s.priority <= heap[j].priority) break;
      heap[i] = heap[j];
      position[heap[i].element] = i;
      i = j;
    }
    heap[i] = s;
    position[s.element] = i;
  }

  private void reallocate() {
    heap = Arrays.copyOf(heap, heap.length * 2 + 1);
  }
}
