package carfora.priorityqueue;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Implementation of PriorityQueueStringDouble with an heap structure
 * @author Matteo Carfora
 */
public class PriorityQueueStringDoubleHeap implements
        PriorityQueueStringDouble {

  class StringWithPrior {
    String element;
    double priority;

    public StringWithPrior(String element, double priority) {
      this.element = element;
      this.priority = priority;
    }
  }
  private StringWithPrior[] heap;
  private int size;
  private final HashMap<String, Integer> position;

  public PriorityQueueStringDoubleHeap() {
    heap = new StringWithPrior[16];
    size = 0;
    position = new HashMap<>();
  }

  public PriorityQueueStringDoubleHeap(int initialCapacity) {
    heap = new StringWithPrior[initialCapacity];
    size = 0;
    position = new HashMap<>();
  }

  @Override
  public boolean add(String element, double priority) {
    if (size == heap.length) reallocate();
    if(position.get(element) == null) {
      heap[size++] = new StringWithPrior(element, priority);
      position.put(element, size - 1);
      moveUp(size - 1);
      return true;
    }
    return false;
  }

  @Override
  public String first() {
    if (size == 0) throw new IllegalStateException("Queue is empty");
    return heap[0].element;
  }

  @Override
  public String removeFirst() {
    if (size == 0) throw new IllegalStateException("Queue is empty");
    String first = heap[0].element;
    StringWithPrior last = heap[--size];
    if (size > 0) {
      heap[0] = last;
      moveDown(0);
    }
    return first;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public boolean delete(String element) {
    Integer index = position.get(element);
    if (index != null) {
      StringWithPrior last = heap[--size];
      if (size > 0) {
        heap[index] = last;
        moveDown(index);
      }
      position.remove(element);
      return true;
    }
    return false;
  }

  @Override
  public boolean setPriority(String element, double priority) {
    Integer index = position.get(element);
    if (index != null) {
      heap[index].priority = priority;
      moveDown(index);
      moveUp(index);
      return true;
    }
    return false;
  }

  private void moveUp(int i) {
    StringWithPrior s = heap[i];
    while (i > 0 && s.priority < heap[(i - 1) / 2].priority) {
      heap[i] = heap[(i - 1) / 2];
      position.put(heap[i].element, i);
      i = (i - 1) / 2;
    }
    heap[i] = s;
    position.put(s.element, i);
  }

  private void moveDown(int i) {
    StringWithPrior s = heap[i];
    int j;
    while ((j = 2*i + 1) < size) {
      if (j + 1 < size
              && heap[j + 1].priority < heap[j].priority) j++;
      if (s.priority <= heap[j].priority) break;
      heap[i] = heap[j];
      position.put(heap[i].element, i);
      i = j;
    }
    heap[i] = s;
    position.put(s.element, i);
  }

  private void reallocate() {
    heap = Arrays.copyOf(heap, heap.length * 2 + 1);
  }
}
