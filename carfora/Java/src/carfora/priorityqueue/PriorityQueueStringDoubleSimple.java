package carfora.priorityqueue;

import java.util.LinkedList;

/**
 * Implementation of PriorityQueueStringDouble with an ordered list
 * @author Matteo Carfora
 */

public class PriorityQueueStringDoubleSimple implements
        PriorityQueueStringDouble {

  class StringWithPrior {
    String element;
    double priority;

    public StringWithPrior(String element, double priority) {
      this.element = element;
      this.priority = priority;
    }

    // used for String checks inside LinkedList<StringWithPrior>
    public StringWithPrior(String element) {
      this(element, Double.NaN);
    }

    @Override
    public boolean equals(Object o) {
      return o instanceof StringWithPrior ?
              ((StringWithPrior) o).element.equals(element) : false;
    }
  }
  private LinkedList<StringWithPrior> list;

  public PriorityQueueStringDoubleSimple() {
    list = new LinkedList<>();
  }

  @Override
  public boolean add(String element, double priority) {
    StringWithPrior s = new StringWithPrior(element, priority);
    if (!list.contains(s)) {
      for (int i = 0; i < list.size(); i++) {
        if (priority <= list.get(i).priority) {
          list.add(i, s);
          return true;
        }
      }
      list.add(s);
      return true;
    }
    return false;
  }

  @Override
  public String first() {
    return list.peek().element;
  }

  @Override
  public String removeFirst() {
    return list.poll().element;
  }

  @Override
  public boolean isEmpty() {
    return list.isEmpty();
  }

  @Override
  public boolean delete(String element) {
    return list.remove(new StringWithPrior(element));
  }

  @Override
  public boolean setPriority(String element, double priority) {
    return delete(element) ? add(element, priority) : false;
  }

}
