package carfora.priorityqueue;

/**
 * Priority queue interface with generic elements and Comparable priorities
 * @author Matteo Carfora
 */
public interface PriorityQueue<E, P extends Comparable<? super P>> {

  /**
   * Inserts an element in the queue
   * @param element element to be inserted
   * @param priority priority of the element
   * @return false if element is already present, true if insertion goes well.
   */
  boolean add(E element, P priority);

  /**
   * Gets the element with the highest priority
   * @return the highest priority element, null otherwise
   */
  E first();

  /**
   * Removes and returns the element with the highest priority
   * @return the highest priority element extracted, null otherwise
   */
  E removeFirst();

  /**
   * Checks if queue is empty
   * @return true if empty, false otherwise
   */
  boolean isEmpty();

  /**
   * Deletes an element in the queue
   * @param element the element to be deleted
   * @return true if deleted, false otherwise
   */
  boolean delete(E element);

  /**
   * Changes priority of an element in the queue
   * @param element the element to be modified
   * @param priority the new priority
   * @return true if priority changed, false otherwise
   */
  boolean setPriority(E element, P priority);
}
