package carfora.priorityqueue;

import java.util.LinkedList;

/**
 * Implementation of PriorityQueue with an ordered list
 * @author Matteo Carfora
 */
public class PriorityQueueSimple<E, P extends Comparable <? super P>> implements
        PriorityQueue<E, P> {

  class ElemWithPrior {
    E element;
    P priority;

    public ElemWithPrior(E element, P priority) {
      this.element = element;
      this.priority = priority;
    }

    // used for element checks inside LinkedList<ElemWithPrior>
    public ElemWithPrior(E element) {
      this(element, null);
    }

    @Override
    public boolean equals(Object o) {
      return ElemWithPrior.class.isInstance(o) ?
              ((ElemWithPrior)o).element.equals(element) : false;
    }
  }
  private LinkedList<ElemWithPrior> list;

  public PriorityQueueSimple() {
    list = new LinkedList<>();
  }

  @Override
  public boolean add(E element, P priority) {
    ElemWithPrior s = new ElemWithPrior(element, priority);
    if (!list.contains(s)) {
      for (int i = 0; i < list.size(); i++) {
        if (priority.compareTo(list.get(i).priority) <= 0) {
          list.add(i, s);
          return true;
        }
      }
      return list.add(s);
    }
    return false;
  }

  @Override
  public E first() {
    return list.peek().element;
  }

  @Override
  public E removeFirst() {
    return list.poll().element;
  }

  @Override
  public boolean isEmpty() {
    return list.isEmpty();
  }

  @Override
  public boolean delete(E element) {
    return list.remove(new ElemWithPrior(element));
  }

  @Override
  public boolean setPriority(E element, P priority) {
    return delete(element) ? add(element, priority) : false;
  }

}
