package carfora.unionfind;

import java.util.HashMap;

/**
 * Implementation of generic union-find structure
 * with union by rank with path compression
 * @author Matteo Carfora
 * @param <T> type of elements in structure
 */
public class UnionFind<T> {

  private static class Node<T> {
    final T elem;
    Node<T> parent;
    int rank;

    Node (T elem) {
      this.elem = elem;
      this.rank = 0;
    }
  }

  private final HashMap<T, Node<T>> elemMap;

  private Node<T> findRoot(Node<T> node) {
    Node<T> root = node;
    while (root.parent != null) root = root.parent;
    while (node.parent != null) {
      Node<T> tmp = node.parent;
      node.parent = root;
      node = tmp;
    }
    return root;
  }

  UnionFind() {
    elemMap = new HashMap<>();
  }

  /**
   * Creates a new set containing only elem
   * @param elem the element of the new set
   * @exception IllegalArgumentException if element is already contained in
   * another set
   */
  public void makeSet(T elem) {
    if (elemMap.containsKey(elem))
      throw new IllegalArgumentException("Element already present");
    elemMap.put(elem, new Node<>(elem));
  }

  /**
   * Searches the representative element of the set of the passed argument
   * @param elem the element to be searched
   * @return the representative element of the set, null if the element
   * is not present in any subset
   */
  public T find(T elem) {
    Node<T> e = elemMap.get(elem);
    return e != null ? findRoot(e).elem : null;
  }

  /**
   * Kruskal-Union
   * Joins the subsets of the passed arguments
   * @param a an element of the first subset
   * @param b an element of the second subset
   * @return false if the elements are from the same subset or one of them
   * is not present in any subset and does nothing,
   * otherwise joins the subsets and returns true
   */
  public boolean union(T a, T b) {
    Node<T> nd1 = elemMap.get(a), nd2 = elemMap.get(b);
    if (nd1 == null || nd2 == null) return false;
    Node<T> rep1 = findRoot(nd1), rep2 = findRoot(nd2);
    if (rep1 == rep2) return false;

    if (rep1.rank < rep2.rank) {
      rep1.parent = rep2;
    } else if (rep1.rank > rep2.rank) {
      rep2.parent = rep1;
    } else {
      rep2.parent = rep1;
      rep1.rank++;
    }
    return true;
  }

}
