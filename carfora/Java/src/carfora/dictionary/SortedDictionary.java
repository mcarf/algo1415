package carfora.dictionary;

/**
 * Dictionary interface with generic keys and values
 * with total ordering on its keys
 * @author Matteo Carfora
 * @param <K> type of keys
 * @param <V> type of values
 */
public interface SortedDictionary<K extends Comparable<? super K>,V>
  extends Dictionary<K,V> {

  /**
   * Returns the minimum key in the dictionary
   */
  K minKey();

  /**
   * Returns the maximum key in the dictionary
   */
  K maxKey();

  /**
   * Returns the value associated to the minimum key in the dictionary
   */
  V elementOfMinKey();

  /**
   * Returns the value associated to the maximum key in the dictionary
   */
  V elementOfMaxKey();
}
