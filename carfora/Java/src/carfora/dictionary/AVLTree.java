package carfora.dictionary;

/**
 * Implementation of SortedDictionary with an AVL tree structure
 * with "empty" nodes as objects and iterative algorithms
 * @author Matteo Carfora
 */
public class AVLTree<K extends Comparable<? super K>, V> implements
        SortedDictionary<K,V> {

  class Node {
    K key;
    V value;
    Node parent, left, right;
    int height;

    // "empty" node
    Node() {
      key = null; value = null;
      parent = left = right = null;
      height = -1;
    }

    void setToLeaf(K key, V value) {
      this.key = key;
      this.value = value;
      this.height = 0;
      this.left = new Node();
      this.right = new Node();
      this.left.parent = this.right.parent = this;
    }

    void setTo(Node nd) {
      this.key = nd.key;
      this.value = nd.value;
      this.parent = nd.parent;
      this.left = nd.left;
      if (left != null) left.parent = this;
      this.right = nd.right;
      if (right != null) right.parent = this;
      this.height = nd.height;
    }

    void setKeyValueTo(K key, V value) {
      this.key = key;
      this.value = value;
    }

    boolean isEmpty() {
      return left == null && right == null;
    }

    /**
     * Balance factor of the node
     * PRE - node is not 'empty'
     */
    int bfact() {
      return left.height - right.height;
    }

    /**
     * Updates node's height
     * PRE - both children have updated heights
     */
    private void updateHeight() {
      height = 1 + Math.max(left.height, right.height);
    }
  }

  private final Node root;

  public AVLTree () {
    root = new Node();
    root.parent = new Node();
  }

  @Override
  public boolean isEmpty() {
    return root.isEmpty();
  }

  @Override
  public V find(K key) {
    return findNode(key).value;
  }

  @Override
  public V add(K key, V value) {
    Node nd = findNode(key);
    V oldValue = nd.value;
    if (nd.isEmpty()) {
      nd.setToLeaf(key, value);
      singleRebalance(nd.parent);
    }
    else nd.value = value;
    return oldValue;
  }

  @Override
  public void remove(K key) {
    Node nd = findNode(key);
    if (!nd.isEmpty()) {
      if (nd.left.isEmpty()) {
        nd.right.parent = nd.parent;
        nd.setTo(nd.right);
        multiRebalance(nd.parent);
      }
      else if (nd.right.isEmpty()) {
        nd.left.parent = nd.parent;
        nd.setTo(nd.left);
        multiRebalance(nd.parent);
      }
      else {
        Node extracted = extractMin(nd.right);
        nd.setKeyValueTo(extracted.key, extracted.value);
        multiRebalance(extracted.parent);
      }
    }
  }

  @Override
  public K minKey() {
    return min(root).key;
  }

  @Override
  public K maxKey() {
    return max(root).key;
  }

  @Override
  public V elementOfMinKey() {
    return min(root).value;
  }

  @Override
  public V elementOfMaxKey() {
    return max(root).value;
  }

  /**
   * Finds node with given key
   * @param key the key of the node
   * @return the node with the key associated
   */
  private Node findNode(K key) {
    Node nd = root;
    while(!nd.isEmpty()) {
      int comp = key.compareTo(nd.key);
      if (comp < 0) nd = nd.left;
      else if (comp > 0) nd = nd.right;
      else return nd;
    }
    return nd;
  }

  private Node min(Node nd) {
    while (!nd.left.isEmpty()) nd = nd.left;
    return nd;
  }

  private Node max(Node nd) {
    while (!nd.right.isEmpty()) nd = nd.right;
    return nd;
  }

  /**
   * Extracts minimum node in given subtree
   * PRE - nd is not 'empty'
   * @param nd the root node of the subtree
   * @return the extracted node
   */
  private Node extractMin(Node nd) {
    while (!nd.left.isEmpty()) nd = nd.left;
    Node oldNd = new Node();
    oldNd.setTo(nd);
    nd.right.parent = nd.parent;
    nd.setTo(nd.right);
    return oldNd;
  }

  /**
   * Single rebalancement (insertion)
   * Stops after one rotation or after all heights in path p -> root
   * are updated
   */
  private void singleRebalance(Node p) {
    // temporary check node
    Node n = null;
    // if n != null a rotation has been made after the switch
    while (!p.isEmpty() && n == null) {
      switch (p.bfact()) {
        case 2:
          n = p.left;
          if (n.bfact() >= 0) ll(p);
          else lr(p);
          break;
        case -2:
          n = p.right;
          if (n.bfact() <= 0) rr(p);
          else rl(p);
          break;
        default:
          p.updateHeight();
      }
      p = p.parent;
    }
  }

  /**
   * Multiple rebalancements (removement)
   * Stops after all heights are updated in path p -> root,
   * rotating unbalanced nodes meanwhile
   */
  private void multiRebalance(Node p) {
    while (!p.isEmpty()) {
      switch (p.bfact()) {
        case 2:
          if (p.left.bfact() >= 0) ll(p);
          else lr(p);
          break;
        case -2:
          if (p.right.bfact() <= 0) rr(p);
          else rl(p);
          break;
        default:
          p.updateHeight();
      }
      p = p.parent;
    }
  }

  // Simple rotations
  private void ll(Node nd) {
    Node oldNd = new Node();
    oldNd.setTo(nd);
    nd.setTo(nd.left);
    nd.parent = oldNd.parent;
    oldNd.left.setTo(nd.right);
    oldNd.left.parent = oldNd;
    nd.right.setTo(oldNd);
    nd.right.parent = nd;

    nd.right.updateHeight();
    nd.updateHeight();
  }

  private void rr(Node nd) {
    Node oldNd = new Node();
    oldNd.setTo(nd);
    nd.setTo(nd.right);
    nd.parent = oldNd.parent;
    oldNd.right.setTo(nd.left);
    oldNd.right.parent = oldNd;
    nd.left.setTo(oldNd);
    nd.left.parent = nd;

    nd.left.updateHeight();
    nd.updateHeight();
  }

  // Double rotations
  private void lr(Node nd) {
    rr(nd.left);
    ll(nd);
  }

  private void rl(Node nd) {
    ll(nd.right);
    rr(nd);
  }
}
