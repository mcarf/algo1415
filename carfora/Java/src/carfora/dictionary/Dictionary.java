package carfora.dictionary;

/**
 * Dictionary interface with generic keys and values
 * @author Matteo Carfora
 * @param <K> type of keys
 * @param <V> type of values
 */
public interface Dictionary<K,V> {

  /**
   * Checks if dictionary is empty
   * @return true iff dictionary is empty
   */
  boolean isEmpty();

  /**
   * Searches a key-value pair in the dictionary
   * @param key the key to be searched
   * @return the value associated with the key, null if key is not present
   */
  V find(K key);

  /**
   * Associates a value to a key and adds it to the dictionary.
   * If the key was already in the dictionary, the old k-v pair is replaced
   * @param key the key which the value is to be associated
   * @param value the value to be associated with the key
   * @return the old value if it was present, null otherwise
   */
  V add(K key, V value);

  /**
   * Removes the key-value pair from the dictionary, or it does nothing
   * if the key is not present
   * @param key the key to be deleted
   */
  void remove(K key);
}
