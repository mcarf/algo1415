package carfora.graphs;

/**
 * Graph search algorithms interface
 * @author Matteo Carfora
 * @param <V> type of vertices
 * @param <E> type of edge data
 */
public interface GraphSearch<V,E> {
  /**
   * Main method of graph search implementations
   * @param graph graph to be searched
   * @param source initial vertex  PRE - source is contained in graph
   * @param callback defines what to do on visit
   */
  void search(Graph<V,E> graph, V source, SearchCallback<V,E> callback);
}
