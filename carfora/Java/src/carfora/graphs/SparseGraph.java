package carfora.graphs;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Implementation of Graph interface of a sparse oriented graph
 * with adjacency lists
 * @author Matteo Carfora
 */
public class SparseGraph<V,E> implements Graph<V,E> {

  class Edge {
    V vertex;
    E data;

    Edge(V vertex, E data) {
      this.vertex = vertex;
      this.data = data;
    }
  }

  private final HashMap<V, LinkedList<Edge>> adjacents;

  public SparseGraph() {
    adjacents = new HashMap<>();
  }

  @Override
  public boolean addVertex(V vertex) {
    if (!adjacents.containsKey(vertex)) {
      adjacents.put(vertex, new LinkedList<Edge>());
      return true;
    }
    return false;
  }

  @Override
  public boolean addEdge(V vertex1, V vertex2, E data) {
    if (!hasEdge(vertex1, vertex2)) {
      adjacents.get(vertex1).add(new Edge(vertex2, data));
      return true;
    }
    return false;
  }

  @Override
  public boolean hasVertex(V vertex) {
    return adjacents.containsKey(vertex);
  }

  @Override
  public boolean hasEdge(V vertex1, V vertex2) {
    LinkedList<Edge> adj = adjacents.get(vertex1);
    if (adj == null) return false;
    for (Edge e : adj) {
      if (e.vertex.equals(vertex2)) return true;
    }
    return false;
  }

  @Override
  public E getData(V vertex1, V vertex2) {
    LinkedList<Edge> adj = adjacents.get(vertex1);
    if (adj == null) return null;
    for (Edge e : adj) {
      if (e.vertex.equals(vertex2)) return e.data;
    }
    return null;
  }

  @Override
  public Collection<V> getVertices() {
    return adjacents.keySet();
  }

  @Override
  public Collection<V> getNeighbors(V vertex) {
    LinkedList<V> neighbors = new LinkedList<>();
    LinkedList<Edge> adj = adjacents.get(vertex);
    if (adj == null) return null;
    for (Edge e : adj) {
      neighbors.add(e.vertex);
    }
    return neighbors;
  }

}
