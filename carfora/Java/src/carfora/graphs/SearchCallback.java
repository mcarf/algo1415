package carfora.graphs;

/**
 * Search callback interface
 * implementations of this interface should define what to do when traversing
 * vertices and/or edges in a graph
 * @author Matteo Carfora
 */
public interface SearchCallback<V,E> {
  void onVisitingVertex(V vertex);
  void onTraversingEdge(V source, V dest, E info);
}
