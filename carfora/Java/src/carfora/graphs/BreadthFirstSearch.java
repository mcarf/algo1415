package carfora.graphs;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Breadth first search algorithm
 * @author Matteo Carfora
 */
public class BreadthFirstSearch<V,E> implements GraphSearch<V,E> {

  HashMap<V, Boolean> visited;

  @Override
  public void search(Graph<V,E> graph, V source, SearchCallback<V,E> callback) {
    visited = new HashMap<>();
    for (V vertex : graph.getVertices()) visited.put(vertex, false);
    searchReachable(graph, source, callback);

    // visit source unreachable nodes
    for (V vertex : graph.getVertices()) {
      if (!visited.get(vertex))
        searchReachable(graph, vertex, callback);
    }
  }

  private void searchReachable(Graph<V,E> graph, V source,
          SearchCallback<V,E> callback) {
    Queue<V> q = new LinkedList<>();
    q.add(source);
    visited.put(source, true);
    while(!q.isEmpty()) {
      V vertex = q.remove();
      callback.onVisitingVertex(vertex);
      for (V v : graph.getNeighbors(vertex)) {
        if (!visited.get(v)) {
          q.add(v);
          visited.put(v, true);
          callback.onTraversingEdge(vertex, v, graph.getData(vertex, v));
        }
      }
    }
  }

}
