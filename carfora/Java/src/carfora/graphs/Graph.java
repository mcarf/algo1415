package carfora.graphs;

import java.util.Collection;

/**
 * Graph interface with generic vertices and edges
 * @author Matteo Carfora
 * @param <V> type of vertices in the graph
 * @param <E> type of edge associated information
 */
public interface Graph<V,E> {

  /**
   * Adds a new vertex to the graph if it isn't already contained
   * @param vertex the vertex to be added
   * @return true iff the vertex is added
   */
  boolean addVertex(V vertex);

  /**
   * Adds the edge (vertex1, vertex2) to the graph if it isn't
   * already contained
   * @param vertex1 the first vertex of the edge
   * @param vertex2 the second vertex of the edge
   * @param data the data associated with the edge
   * @return true iff the edge is added
   */
  boolean addEdge(V vertex1, V vertex2, E data);

  /**
   * Tells if a vertex is in the graph
   * @param vertex the vertex to be searched
   * @return true iff the vertex is contained in the graph
   */
  boolean hasVertex(V vertex);

  /**
   * Tells if an edge is in the graph
   * @param vertex1 the first vertex of the edge
   * @param vertex2 the second vertex of the edge
   * @return true iff the edge is contained in the graph
   */
  boolean hasEdge(V vertex1, V vertex2);

  /**
   * Gets the data associated to an an edge
   * @param vertex1 the first vertex of the edge
   * @param vertex2 the second vertex of the edge
   * @return the data of the edge, or null if the edge doesn't exist
   */
  E getData(V vertex1, V vertex2);

  /**
   * Gets the vertices of the graph
   * @return a list with the vertices contained in the graph
   */
  Collection<V> getVertices();

  /**
   * Gets the adjacency list of a vertex
   * @param vertex the vertex to be searched
   * @return the adjacency list associated to the vertex or
   * null if it the vertex is not contained in the graph
   */
  Collection<V> getNeighbors(V vertex);
}
