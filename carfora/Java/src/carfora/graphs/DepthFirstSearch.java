package carfora.graphs;

import java.util.HashMap;

/**
 * Depth first search algorithm
 * @author Matteo Carfora
 */
public class DepthFirstSearch<V,E> implements GraphSearch<V,E> {

  private HashMap<V, Boolean> visited;

  @Override
  public void search(Graph<V,E> graph, V source, SearchCallback<V,E> callback) {
    visited = new HashMap<>();
    for (V vertex : graph.getVertices()) visited.put(vertex, false);
    searchReachableRec(graph, source, callback);

    // visit source unreachable nodes
    for (V vertex : graph.getVertices()) {
      if (!visited.get(vertex))
        searchReachableRec(graph, vertex, callback);
    }
  }

  private void searchReachableRec(Graph<V,E> graph, V source,
          SearchCallback<V,E> callback) {
    visited.put(source, true);
    callback.onVisitingVertex(source);
    for (V v : graph.getNeighbors(source)) {
      if (!visited.get(v)) {
        callback.onTraversingEdge(source, v, graph.getData(source, v));
        searchReachableRec(graph, v, callback);
      }
    }
  }

}
