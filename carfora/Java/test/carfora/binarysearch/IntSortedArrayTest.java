package carfora.binarysearch;

import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matteo Carfora
 */

public class IntSortedArrayTest {
  private IntSortedArray fixture;

  @Before
  public void setUp() {
    fixture = new IntSortedArray(new int[]{23, 35, 45, 67, 87});
  }

  /**
   * Test of constructor without parameters of class IntSortedArray.
   */
  @Test
  public void testConstructorWithoutParameters() {
    System.out.println("constructor without parameters");
    IntSortedArray arr = new IntSortedArray();
    assertEquals("Initial capacity is 16", 16, arr.elements.length);
    assertEquals("Initial size is 0", 0, arr.size);
  }

  /**
   * Test of constructor with initialCapacity < 0 of class IntSortedArray.
   */
  @Test(expected = IllegalArgumentException.class)
  public void testConstructorWithCapacityException() {
    System.out.println("constructor with initial capacity exception");
    IntSortedArray arr = new IntSortedArray(-2);
  }

  /**
   * Test of constructor with initialCapacity >= 0 of class IntSortedArray.
   */
  @Test
  public void testConstructorWithCapacity() {
    System.out.println("constructor with initial capacity > 0");
    IntSortedArray arr = new IntSortedArray(30);
    assertEquals("Initial capacity is 30", 30, arr.elements.length);
    assertEquals("Initial size is 0", 0, arr.size);
  }

  /**
   * Test of constructor with unsorted array of class IntSortedArray.
   */
  @Test
  public void testConstructorWithArray() {
    System.out.println("constructor with unsorted array");
    int[] arr = {3, 8, 23, 85, 12, 17, 10};
    IntSortedArray sortedArr = new IntSortedArray(arr);
    assertEquals("Capacity is array length + 16", arr.length + 16,
            sortedArr.elements.length);
    assertEquals("Size is array length", arr.length, sortedArr.size);
    Arrays.sort(arr);
    assertArrayEquals("Internal array contains the passed array sorted",
            arr, Arrays.copyOf(sortedArr.elements, arr.length));
  }

  /**
   * Test of size method, of class IntSortedArray.
   */
  @Test
  public void testSize() {
    System.out.println("size");
    assertEquals("Size is returned", fixture.size, fixture.size());
  }

  /**
   * Test of binarySearch without inserted elements of class IntSortedArray.
   */
  @Test
  public void testBinarySearchEmpty() {
    System.out.println("binarySearch with empty array");
    IntSortedArray arr = new IntSortedArray();
    assertEquals("Value with empty array is -1", -1, arr.binarySearch(23));
  }

  /**
   * Test of binarySearch method of class IntSortedArray.
   */
  @Test
  public void testBinarySearch() {
    System.out.println("binarySearch");
    // expected index while searching 35 in fixture
    int expectedPresent = 1;
    // expected negative index while searching 54 in fixture
    int expectedNotPresent = -4;
    assertEquals("Value is present",
            expectedPresent, fixture.binarySearch(35));
    assertEquals("Value is not present",
            expectedNotPresent, fixture.binarySearch(54));
  }

  /**
   * Test of reallocate method, of class IntSortedArray.
   */
  @Test
  public void testReallocate() {
    System.out.println("reallocate");
    int[] arr = fixture.elements;
    int expected = arr.length * 2 + 1;
    fixture.reallocate();
    assertEquals("Internal array has increased capacity", expected,
            fixture.elements.length);
    assertArrayEquals("Internal array has same elements", arr,
            Arrays.copyOf(fixture.elements, arr.length));
  }

  /**
   * Test of insert method, of class IntSortedArray.
   */
  @Test
  public void testInsert() {
    System.out.println("insert");
    int x = 40, expIndex = 2;
    int[] arr = {23, 35, 40, 45, 67, 87};
    int returnedPos = fixture.insert(x);
    assertEquals("x is inserted and its pos returned", expIndex, returnedPos);
    assertArrayEquals("Internal array has shifted elements",
            arr, Arrays.copyOf(fixture.elements, arr.length));
  }

  /**
   * Test of indexOf method, of class IntSortedArray.
   */
  @Test
  public void testIndexOf() {
    System.out.println("indexOf");
    int present = 45;
    int notPresent = 40;
    assertEquals("Value present and its index is returned",
            2, fixture.indexOf(present));
    assertEquals("Value not present and -1 returned",
            -1, fixture.indexOf(notPresent));
  }

  /**
   * Test of get method with index outside of filled internal array bounds,
   * of class IntSortedArray.
   */
  @Test(expected = ArrayIndexOutOfBoundsException.class)
  public void testGetException() {
    System.out.println("get with index outside of bounds");
    fixture.get(5);
  }

  /**
   * Test of get method with index inside of filled internal array bounds,
   * of class IntSortedArray.
   */
  @Test
  public void testGet() {
    System.out.println("get with index inside of bounds");
    assertEquals("Value returned is in passed position", 45, fixture.get(2));
  }

  /**
   * Test of toString method, of class IntSortedArray.
   */
  @Test
  public void testToString() {
    System.out.println("toString");
    String expResult = "[23, 35, 45, 67, 87]";
    assertEquals("Internal array content as formatted string",
            expResult, fixture.toString());
  }

}
