package carfora.binarysearch;

import java.math.BigInteger;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matteo Carfora
 */
public class SortedArrayListTest {
  private SortedArrayList<BigInteger> fixture;

  @Before
  public void setUp() {
    String values[] = {"23", "35", "45", "67", "87"};
    BigInteger[] arr = new BigInteger[values.length];
    for (int i = 0; i < arr.length; i++) {
      arr[i] = new BigInteger(values[i]);
    }
    fixture = new SortedArrayList<>(arr);
  }

  /**
   * Test of constructor with and without capacity of class SortedArrayList.
   */
  @Test
  public void testConstructorCapacity() {
    System.out.println("constructor with and without initial capacity");
    SortedArrayList arr = new SortedArrayList();
    assertEquals("Initial size is 0", 0, arr.elements.size());
    arr = new SortedArrayList(30);
    assertEquals("Initial size is 0", 0, arr.elements.size());
  }

  /**
   * Test of constructor with unsorted array of class SortedArrayList.
   */
  @Test
  public void testConstructorArray() {
    System.out.println("constructor with unsorted array");
    String values[] = {"3", "8", "23", "85", "12", "17", "10"};
    BigInteger[] arr = new BigInteger[values.length];
    for (int i = 0; i < arr.length; i++) {
      arr[i] = new BigInteger(values[i]);
    }
    SortedArrayList<BigInteger> sortedArr = new SortedArrayList<>(arr);
    assertEquals("Size is array length", arr.length, sortedArr.elements.size());
    Arrays.sort(arr);
    assertArrayEquals("Internal array contains the passed array sorted",
            arr, Arrays.copyOf(sortedArr.elements.toArray(), arr.length));
  }

    /**
   * Test of binarySearch without inserted elements of class SortedArrayList.
   */
  @Test
  public void testBinarySearchEmpty() {
    System.out.println("binarySearch with empty array");
    SortedArrayList<BigInteger> arr = new SortedArrayList<>();
    BigInteger value = new BigInteger("23");
    assertEquals("Value with empty array is -1", -1, arr.binarySearch(value));
  }

  /**
   * Test of binarySearch method of class SortedArrayList.
   */
  @Test
  public void testBinarySearch() {
    System.out.println("binarySearch");

    // expected index while searching 35 in fixture
    int expectedPosition = 1;
    BigInteger value = new BigInteger("35");

    assertEquals("Value is present", expectedPosition,
            fixture.binarySearch(value));

    // expected negative index while searching 54 in fixture
    expectedPosition = -4;
    value = new BigInteger("54");

    assertEquals("Value is not present", expectedPosition,
            fixture.binarySearch(value));
  }

  /**
   * Test of insert method, of class SortedArrayList.
   */
  @Test
  public void testInsert() {
    System.out.println("insert");
    BigInteger value = new BigInteger("40");
    int expIndex = 2;
    int returnedPos = fixture.insert(value);
    assertEquals("x is inserted and its pos returned", expIndex, returnedPos);
  }

   /**
   * Test of indexOf method, of class IntSortedArray.
   */
  @Test
  public void testIndexOf() {
    System.out.println("indexOf");
    BigInteger present = new BigInteger("45");
    BigInteger notPresent = new BigInteger("40");
    assertEquals("Value present and its index is returned",
            2, fixture.indexOf(present));
    assertEquals("Value not present and -1 returned",
            -1, fixture.indexOf(notPresent));
  }
}
