package carfora.graphs;

import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matteo Carfora
 */
public class DepthFirstSearchTest {

  Graph<String, Void> fixture;
  DepthFirstSearch<String, Void> dfs;
  SearchCallback<String, Void> callback;
  // values are parent of keys
  HashMap<String, String> parent;

  @Before
  public void setUp() {
    fixture = new SparseGraph<>();
    fixture.addVertex("Torino");
    fixture.addVertex("Milano");
    fixture.addVertex("Genova");
    fixture.addVertex("Firenze");
    fixture.addVertex("Bologna");
    fixture.addVertex("Venezia");
    fixture.addEdge("Torino", "Milano", null);
    fixture.addEdge("Torino", "Genova", null);
    fixture.addEdge("Torino", "Firenze", null);
    fixture.addEdge("Milano", "Venezia", null);
    fixture.addEdge("Milano", "Firenze", null);
    fixture.addEdge("Genova", "Firenze", null);
    fixture.addEdge("Firenze", "Milano", null);
    fixture.addEdge("Firenze", "Bologna", null);
    fixture.addEdge("Venezia", "Bologna", null);
    fixture.addEdge("Bologna", "Venezia", null);
    dfs = new DepthFirstSearch<>();
    parent = new HashMap<>();
    callback = new SearchCallback<String, Void>() {

      @Override
      public void onVisitingVertex(String vertex) {}

      @Override
      public void onTraversingEdge(String source, String dest, Void info) {
        parent.put(dest, source);
      }

    };
  }

  /**
   * Test of search method, of class DepthFirstSearch.
   * This test depends on SparseGraph and DepthFirstSearch implementations,
   * in particular it depends, on each vertex visit, in the choice of
   * the sequence of unvisited adjacent nodes to visit
   */
  @Test
  public void testSearch() {
    System.out.println("search");
    HashMap<String, String> expParent = new HashMap<>();
    expParent.put("Milano", "Torino");
    expParent.put("Venezia", "Milano");
    expParent.put("Bologna", "Venezia");
    expParent.put("Firenze", "Milano");
    expParent.put("Genova", "Torino");
    dfs.search(fixture, "Torino", callback);
    assertTrue("Correct tree structure",
            expParent.entrySet().equals(parent.entrySet()));
  }

}
