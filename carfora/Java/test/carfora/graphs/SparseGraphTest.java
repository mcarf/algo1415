package carfora.graphs;

import java.util.LinkedList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matteo Carfora
 */
public class SparseGraphTest {

  SparseGraph<String, Double> fixture;

  @Before
  public void setUp() {
    fixture = new SparseGraph<>();
    fixture.addVertex("Torino");
    fixture.addVertex("Milano");
    fixture.addEdge("Torino", "Milano", 200.0);
  }

  /**
   * Test of addVertex method, of class SparseGraph.
   */
  @Test
  public void testAddVertex() {
    System.out.println("addVertex");
    assertTrue("New vertex added", fixture.addVertex("Vercelli"));
    assertFalse("Vertex already added", fixture.addVertex("Vercelli"));
  }

  /**
   * Test of addEdge method, of class SparseGraph.
   */
  @Test
  public void testAddEdge() {
    System.out.println("addEdge");
    assertTrue("New edge added", fixture.addEdge("Milano", "Torino", 200.0));
    assertFalse("Edge already added",
            fixture.addEdge("Milano", "Torino", 200.0));
  }

  /**
   * Test of hasVertex method, of class SparseGraph.
   */
  @Test
  public void testHasVertex() {
    System.out.println("hasVertex");
    assertTrue("Vertex contained", fixture.hasVertex("Torino"));
    assertFalse("Vertex not contained", fixture.hasVertex("Vercelli"));
  }

  /**
   * Test of hasEdge method, of class SparseGraph.
   */
  @Test
  public void testHasEdge() {
    System.out.println("hasEdge");
    assertTrue("Edge contained", fixture.hasEdge("Torino", "Milano"));
    assertFalse("Edge not contained", fixture.hasEdge("Milano", "Torino"));
  }

  /**
   * Test of getData method, of class SparseGraph.
   */
  @Test
  public void testGetData() {
    System.out.println("getData");
    assertEquals("Edge data returned",
            200.0, (double)fixture.getData("Torino", "Milano"), 0.1);
    assertNull("Edge does not exist", fixture.getData("Milano", "Torino"));
  }

  /**
   * Test of getVertices method, of class SparseGraph.
   */
  @Test
  public void testGetVertices() {
    System.out.println("getVertices");
    LinkedList<String> vertices = new LinkedList<>();
    vertices.add("Torino");
    vertices.add("Milano");
    assertTrue("Vertices collection",
            vertices.containsAll(fixture.getVertices()));
  }

  /**
   * Test of getNeighbors method, of class SparseGraph.
   */
  @Test
  public void testGetNeighbors() {
    System.out.println("getNeighbors");
    LinkedList<String> neighbors = new LinkedList<>();
    neighbors.add("Milano");
    assertTrue("Neighbors collection",
            neighbors.containsAll(fixture.getNeighbors("Torino")));
  }

}
