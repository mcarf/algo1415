package carfora.unionfind;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matteo Carfora
 */
public class UnionFindTest {

  private UnionFind<String> fixture;

  @Before
  public void setUp() {
    fixture = new UnionFind<>();
    fixture.makeSet("Sip");
    fixture.makeSet("Stipel");
    fixture.makeSet("Fiat");
    fixture.makeSet("Lancia");
    fixture.makeSet("Innocenti");

    fixture.union("Sip", "Stipel");
    fixture.union("Lancia", "Innocenti");
  }


  /**
   * Test of makeSet exception, of class UnionFind.
   */
  @Test(expected = IllegalArgumentException.class)
  public void testMakeSetException() {
    fixture.makeSet("Fiat");
  }

  /**
   * Test of makeSet method, of class UnionFind.
   */
  @Test
  public void testMakeSet() {
    System.out.println("makeSet");
    fixture.makeSet("Telecom");
    assertEquals("Element is added", fixture.find("Telecom"), "Telecom");
  }

  /**
   * Test of find method, of class UnionFind.
   */
  @Test
  public void testFind() {
    System.out.println("find");
    assertEquals("Representative is found", fixture.find("Stipel"), "Sip");
  }

  /**
   * Test of union method, of class UnionFind.
   */
  @Test
  public void testUnion() {
    System.out.println("union");
    assertTrue("Subsets joined", fixture.union("Fiat", "Lancia"));
    assertEquals("Same representative",
            fixture.find("Lancia"), fixture.find("Fiat"));
  }

}
