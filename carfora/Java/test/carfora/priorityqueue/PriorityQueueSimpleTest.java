package carfora.priorityqueue;

import java.math.BigInteger;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Matteo Carfora
 */
public class PriorityQueueSimpleTest {

  PriorityQueue<String, BigInteger> fixture;

  @Before
  public void setUp() {
    fixture = new PriorityQueueSimple<>();
    fixture.add("prova", BigInteger.valueOf(1));
    fixture.add("test", BigInteger.valueOf(3));
  }

  /**
   * Test of add method, of class PriorityQueueSimple.
   */
  @Test
  public void testAdd() {
    System.out.println("add");
    assertTrue("Element is added",
            fixture.add("addTest", BigInteger.valueOf(2)));
    assertFalse("Element is already inserted",
            fixture.add("addTest", BigInteger.valueOf(2)));
  }

  /**
   * Test of first method, of class PriorityQueueSimple.
   */
  @Test
  public void testFirst() {
    System.out.println("first");
    assertEquals("First element in q returned", fixture.first(), "prova");
  }

  /**
   * Test of removeFirst method, of class PriorityQueueSimple.
   */
  @Test
  public void testRemoveFirst() {
    System.out.println("removeFirst");
    String expected = fixture.removeFirst();
    assertEquals("First element in q returned..", expected, "prova");
    assertEquals("..and removed", fixture.first(), "test");
  }

  /**
   * Test of isEmpty method, of class PriorityQueueSimple.
   */
  @Test
  public void testIsEmpty() {
    System.out.println("isEmpty");
    assertFalse("q is not empty", fixture.isEmpty());
    fixture.removeFirst();
    fixture.removeFirst();
    assertTrue("q is empty", fixture.isEmpty());
  }

  /**
   * Test of delete method, of class PriorityQueueSimple.
   */
  @Test
  public void testDelete() {
    System.out.println("delete");
    assertTrue("Element in q is deleted", fixture.delete("prova"));
    assertFalse("Element in q is not present", fixture.delete("prova"));
  }

  /**
   * Test of setPriority method, of class PriorityQueueSimple.
   */
  @Test
  public void testSetPriority() {
    System.out.println("setPriority");
    fixture.setPriority("prova", BigInteger.valueOf(4));
    assertEquals("Element has lower priority", fixture.removeFirst(), "test");
    assertEquals("Element's new position", fixture.first(), "prova");
  }

}