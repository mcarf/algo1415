package carfora.priorityqueue;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Matteo Carfora
 */
public class PriorityQueueIntDoubleTest {

  PriorityQueueIntDouble fixture;

  @Before
  public void setUp() {
    fixture = new PriorityQueueIntDouble(10);
    fixture.add(1, 1.0);
    fixture.add(3, 3.0);
  }

  /**
   * Test of add method, of class PriorityQueueHeap.
   */
  @Test
  public void testAdd() {
    System.out.println("add");
    assertTrue("Element is added",
            fixture.add(2, 2.0));
    assertFalse("Element is already inserted",
            fixture.add(2, 2.0));
  }

  /**
   * Test of first method, of class PriorityQueueHeap.
   */
  @Test
  public void testFirst() {
    System.out.println("first");
    assertEquals("First element in q returned", fixture.first(), 1);
  }

  /**
   * Test of removeFirst method, of class PriorityQueueHeap.
   */
  @Test
  public void testRemoveFirst() {
    System.out.println("removeFirst");
    int expected = fixture.removeFirst();
    assertEquals("First element in q returned..", expected, 1);
    assertEquals("..and removed", fixture.first(), 3);
  }

  /**
   * Test of isEmpty method, of class PriorityQueueHeap.
   */
  @Test
  public void testIsEmpty() {
    System.out.println("isEmpty");
    assertFalse("q is not empty", fixture.isEmpty());
    fixture.removeFirst();
    fixture.removeFirst();
    assertTrue("q is empty", fixture.isEmpty());
  }

  /**
   * Test of delete method, of class PriorityQueueHeap
   */
  @Test
  public void testDelete() {
    System.out.println("delete");
    assertTrue("Element in q is deleted", fixture.delete(1));
    assertFalse("Element in q is not present", fixture.delete(1));
  }

  /**
   * Test of setPriority method, of class PriorityQueueHeap.
   */
  @Test
  public void testSetPriority() {
    System.out.println("setPriority");
    fixture.setPriority(1, 4.0);
    assertEquals("Element has lower priority", fixture.removeFirst(), 3);
    assertEquals("Element's new position", fixture.first(), 1);
  }

}