package carfora.priorityqueue;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Matteo Carfora
 */
public class PriorityQueueStringDoubleHeapTest {

  PriorityQueueStringDouble fixture;

  @Before
  public void setUp() {
    fixture = new PriorityQueueStringDoubleHeap();
    fixture.add("prova", 1.0);
    fixture.add("test", 2.0);
  }

  /**
   * Test of add method, of class PriorityQueueStringDoubleHeap.
   */
  @Test
  public void testAdd() {
    System.out.println("add");
    assertTrue("Element is added", fixture.add("addTest", 1.5));
    assertFalse("Element is already inserted", fixture.add("addTest", 1.5));
  }

  /**
   * Test of first method, of class PriorityQueueStringDoubleHeap.
   */
  @Test
  public void testFirst() {
    System.out.println("first");
    assertEquals("First element in q returned", fixture.first(), "prova");
  }

  /**
   * Test of removeFirst method, of class PriorityQueueStringDoubleHeap.
   */
  @Test
  public void testRemoveFirst() {
    System.out.println("removeFirst");
    String expected = fixture.removeFirst();
    assertEquals("First element in q returned..", expected, "prova");
    assertEquals("..and removed", fixture.first(), "test");
  }

  /**
   * Test of isEmpty method, of class PriorityQueueStringDoubleHeap.
   */
  @Test
  public void testIsEmpty() {
    System.out.println("isEmpty");
    assertFalse("q is not empty", fixture.isEmpty());
    fixture.removeFirst();
    fixture.removeFirst();
    assertTrue("q is empty", fixture.isEmpty());
  }

  /**
   * Test of delete method, of class PriorityQueueStringDoubleHeap.
   */
  @Test
  public void testDelete() {
    System.out.println("delete");
    assertTrue("Element in q is deleted", fixture.delete("prova"));
    assertFalse("Element in q is not present", fixture.delete("prova"));
  }

  /**
   * Test of setPriority method, of class PriorityQueueStringDoubleHeap.
   */
  @Test
  public void testSetPriority() {
    System.out.println("setPriority");
    fixture.setPriority("prova", 3.0);
    assertEquals("Element has lower priority", fixture.removeFirst(), "test");
    assertEquals("Element's new position", fixture.first(), "prova");
  }

}
