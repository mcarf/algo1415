package carfora.dictionary;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matteo Carfora
 */
public class AVLTreeTest {

  private AVLTree<String, String>  fixture;

  @Before
  public void setUp() {
    fixture = new AVLTree<>();
    fixture.add("ciao", "test1");
    fixture.add("sort", "test2");
    fixture.add("prova", "test3");
    fixture.add("test", "test4");
  }

  /**
   * Test of isEmpty method, of class AVLTree.
   */
  @Test
  public void testIsEmpty() {
    System.out.println("isEmpty");
    assertFalse("Tree is not empty", fixture.isEmpty());
    fixture.remove("ciao");
    fixture.remove("sort");
    fixture.remove("prova");
    fixture.remove("test");
    assertTrue("Tree is empty", fixture.isEmpty());
  }

  /**
   * Test of find method, of class AVLTree.
   */
  @Test
  public void testFind() {
    System.out.println("find");
    assertEquals("Key found", fixture.find("test"), "test4");
    assertNull("Key not found", fixture.find("pippo"));
  }

  /**
   * Test of add method value replacement, of class AVLTree.
   */
  @Test
  public void testAddReplacement() {
    System.out.println("add replacement");
    assertEquals("Old value is returned",
            fixture.add("prova", "prova"), "test3");
    assertEquals("Value is updated", fixture.find("prova"), "prova");
  }

  /**
   * test of add method new value insertion, of class AVLTree.
   */
  @Test
  public void testAddNewPair() {
    System.out.println("add new key-value pair");
    assertNull("New pair added", fixture.add("pippo", "test5"));
  }

  /**
   * Test of remove method, of class AVLTree.
   */
  @Test
  public void testRemove() {
    System.out.println("remove");
    fixture.remove("sort");
    assertNull("Value is removed", fixture.find("sort"));
  }

  /**
   * Test of minKey method, of class AVLTree.
   */
  @Test
  public void testMinKey() {
    System.out.println("minKey");
    assertEquals("Min key", fixture.minKey(), "ciao");
  }

  /**
   * Test of maxKey method, of class AVLTree.
   */
  @Test
  public void testMaxKey() {
    System.out.println("maxKey");
    assertEquals("Max key", fixture.maxKey(), "test");
  }

  /**
   * Test of elementOfMinKey method, of class AVLTree.
   */
  @Test
  public void testElementOfMinKey() {
    System.out.println("elementOfMinKey");
    assertEquals("Element of min key", fixture.elementOfMinKey(), "test1");
  }

  /**
   * Test of elementOfMaxKey method, of class AVLTree.
   */
  @Test
  public void testElementOfMaxKey() {
    System.out.println("elementOfMaxKey");
    assertEquals("Element of max key", fixture.elementOfMaxKey(), "test4");
  }

}
