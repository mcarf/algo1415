package carfora.dijkstra;

import carfora.graphs.*;
import java.util.LinkedList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matteo Carfora
 */
public class MinPathDijkstraTest {

  /* in java 8 DoubleSupplier should be implemented as a lambda expression like
   * () -> <double value>;
   * this implementation is java 7 compliant, it is a double primitive wrapper
   * like java.lang.Double that implements DoubleSupplier
   */
  class DoubleAlt implements DoubleSupplier {
    double value;
    public DoubleAlt(double value) {
      this.value = value;
    }

    @Override
    public double getAsDouble() {
      return value;
    }
  }

  Graph<String, DoubleSupplier> fixture;
  MinPathDijkstra<String, DoubleSupplier> mpd;

  @Before
  public void setUp() {
    fixture = new SparseGraph<>();
    fixture.addVertex("Torino");
    fixture.addVertex("Milano");
    fixture.addVertex("Genova");
    fixture.addVertex("Firenze");
    fixture.addVertex("Bologna");
    fixture.addVertex("Venezia");
    fixture.addEdge("Torino", "Milano", new DoubleAlt(200));
    fixture.addEdge("Torino", "Genova", new DoubleAlt(150));
    fixture.addEdge("Torino", "Firenze", new DoubleAlt(300));
    fixture.addEdge("Milano", "Venezia", new DoubleAlt(50));
    fixture.addEdge("Milano", "Firenze", new DoubleAlt(200));
    fixture.addEdge("Genova", "Firenze", new DoubleAlt(200));
    fixture.addEdge("Firenze", "Milano", new DoubleAlt(200));
    fixture.addEdge("Firenze", "Bologna", new DoubleAlt(200));
    fixture.addEdge("Venezia", "Bologna", new DoubleAlt(50));
    fixture.addEdge("Bologna", "Venezia", new DoubleAlt(200));
    mpd = new MinPathDijkstra<>();
  }
  /**
   * Test of minPath method, of class MinPathDijkstra.
   */
  @Test
  public void testMinPath() {
    System.out.println("minPath");
    LinkedList<String> expPath = new LinkedList<>();
    expPath.add("Torino");
    expPath.add("Milano");
    expPath.add("Venezia");
    expPath.add("Bologna");
    assertTrue("Path is correct",
            mpd.minPath(fixture, "Torino", "Bologna").equals(expPath));
  }

}
