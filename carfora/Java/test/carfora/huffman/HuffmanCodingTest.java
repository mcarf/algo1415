package carfora.huffman;

import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Matteo Carfora
 */
public class HuffmanCodingTest {

  HuffmanCoding<Character> fixture;
  HashMap<Character, Double> freqs;

  @Before
  public void setUp() {
    freqs = new HashMap<>();
    freqs.put('a', 0.45);
    freqs.put('b', 0.13);
    freqs.put('c', 0.12);
    freqs.put('d', 0.16);
    freqs.put('e', 0.09);
    freqs.put('f', 0.05);
    fixture = new HuffmanCoding<>();
    fixture.build(freqs);
  }

  /**
   * Test of build method, of class HuffmanCoding.
   */
  @Test
  public void testBuild() {
    System.out.println("build");
    HashMap<Character, String> expMap = new HashMap<>();
    expMap.put('a', "0");
    expMap.put('b', "101");
    expMap.put('c', "100");
    expMap.put('d', "111");
    expMap.put('e', "1101");
    expMap.put('f', "1100");
    assertTrue(expMap.equals(fixture.huffMap));
  }

  /**
   * Test of encode method, of class HuffmanCoding.
   */
  @Test
  public void testEncode() {
    System.out.println("encode");
    ArrayList<Character> textArr = new ArrayList<>();
    BitArray expArr = new BitArray(32);
    String text = "cafebabe";
    String expBitString = "10001100110110101011101";
    for (char c : text.toCharArray())
      textArr.add(c);
    for (char c : expBitString.toCharArray())
      expArr.add(c == '0' ? 0 : 1);
    BitArray arr = fixture.encode(textArr);
    assertTrue(expArr.equals(arr));
  }

  /**
   * Test of decode method, of class HuffmanCoding.
   */
  @Test
  public void testDecode() {
    System.out.println("decode");
    ArrayList<Character> expTextArr = new ArrayList<>();
    BitArray arr = new BitArray(32);
    String expText = "cafebabe";
    String bitString = "10001100110110101011101";
    for (char c : expText.toCharArray())
      expTextArr.add(c);
    for (char c : bitString.toCharArray())
      arr.add(c == '0' ? 0 : 1);
    ArrayList<Character> textArr = fixture.decode(arr);
    assertTrue(expTextArr.equals(textArr));
  }

}
