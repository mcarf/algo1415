package carfora.sort;

import java.math.BigInteger;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Matteo Carfora
 */
public class SortingTest {

  int[] intFixture;
  BigInteger[] biFixture;

  @Before
  public void setUp() {
    intFixture = new int[] {10, 2, 127, 100, 7, 14, 103, 200, 54, 32};
    biFixture = new BigInteger[intFixture.length];
    for (int i = 0; i < intFixture.length; i++) {
      biFixture[i] = BigInteger.valueOf(intFixture[i]);
    }
  }

  /**
   * Test of isSorted method, of class Sorting.
   */
  @Test
  public void testIsSorted_intArr() {
    System.out.println("isSorted int array");
    int[] sortedFixture = {2, 7, 10, 14, 32, 54, 100, 103, 127, 200};
    assertFalse("Array is not sorted", Sorting.isSorted(intFixture));
    assertTrue("Array is sorted", Sorting.isSorted(sortedFixture));
  }

  /**
   * Test of isSorted method, of class Sorting.
   */
  @Test
  public void testIsSorted_GenericType() {
    System.out.println("isSorted generic array");
    int[] sortedFixtureValues = {2, 7, 10, 14, 32, 54, 100, 103, 127, 200};
    BigInteger[] sortedFixture = new BigInteger[sortedFixtureValues.length];
    for (int i = 0; i < sortedFixture.length; i++) {
      sortedFixture[i] = BigInteger.valueOf(sortedFixtureValues[i]);
    }
    assertFalse("Array is not sorted", Sorting.isSorted(biFixture));
    assertTrue("Array is sorted", Sorting.isSorted(sortedFixture));
  }

  /**
   * Test of ssort method, of class Sorting.
   */
  @Test
  public void testSsort() {
    System.out.println("ssort");
    Sorting.ssort(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
    Sorting.ssort(biFixture);
    assertTrue("Generic array is sorted", Sorting.isSorted(biFixture));
  }

  /**
   * Test of isort method, of class Sorting.
   */
  @Test
  public void testIsort() {
    System.out.println("isort");
    Sorting.isort(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
    Sorting.isort(biFixture);
    assertTrue("Generic array is sorted", Sorting.isSorted(biFixture));
  }

  /**
   * Test of isortBin method, of class Sorting.
   */
  @Test
  public void testIsortBin() {
    System.out.println("isortBin");
    Sorting.isortBin(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
  }

  /**
   * Test of msortBasic, of class Sorting.
   */
  @Test
  public void testMsortBasic() {
    System.out.println("msortBasic");
    Sorting.msortBasic(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
  }

  /**
   * Test of msortNoGarbage, of class Sorting.
   */
  @Test
  public void testMsortNoGarbage() {
    System.out.println("msortNoGarbage");
    Sorting.msortNoGarbage(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
    Sorting.msortNoGarbage(biFixture);
    assertTrue("Generic array is sorted", Sorting.isSorted(biFixture));
  }

  /**
   * Test of msortAlt, of class Sorting.
   */
  @Test
  public void testMsortAlt() {
    System.out.println("msortAlt");
    Sorting.msortAlt(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
    Sorting.msortAlt(biFixture);
    assertTrue("Generic array is sorted", Sorting.isSorted(biFixture));
  }

  /**
   * Test of parallelMergesort, of class Sorting.
   */
  @Test
  public void testParallelMergesort() {
    System.out.println("parallelMergesort");
    Sorting.parallelMergesort(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
  }

  /**
   * Test of qsortBasic, of class Sorting.
   */
  @Test
  public void testQsortBasic() {
    System.out.println("qsortBasic");
    Sorting.qsortBasic(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
    Sorting.qsortBasic(biFixture);
    assertTrue("Generic array is sorted", Sorting.isSorted(biFixture));
  }

  /**
   * Test of qsortHoare, of class Sorting.
   */
  @Test
  public void testQSortHoare() {
    System.out.println("qsortHoare");
    Sorting.qsortHoare(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
    Sorting.qsortHoare(biFixture);
    assertTrue("Generic array is sorted", Sorting.isSorted(biFixture));
  }

  /**
   * Test of qsortHoareIsort, of class Sorting.
   */
  @Test
  public void testQSortHoareIsort() {
    System.out.println("qsortHoareIsort");
    Sorting.qsortHoareIsort(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
    Sorting.qsortHoareIsort(biFixture);
    assertTrue("Generic array is sorted", Sorting.isSorted(biFixture));
  }

  /**
   * Test of parallelQuicksort, of class Sorting.
   */
  @Test
  public void testParallelQuicksort() {
    System.out.println("parallelQuicksort");
    Sorting.parallelQuicksort(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
  }

  /**
   * Test of hsort method, of class Sorting.
   */
  @Test
  public void testHsort() {
    System.out.println("hsort");
    Sorting.hsort(intFixture);
    assertTrue("Int array is sorted", Sorting.isSorted(intFixture));
    Sorting.hsort(biFixture);
    assertTrue("Generic array is sorted", Sorting.isSorted(biFixture));
  }
}
